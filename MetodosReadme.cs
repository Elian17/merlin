﻿#region Usuarios

1. AgregarSuperAdmin()

//URL
http://trial-bin.000webhostapp.com/colegio/index.php/usuariomaster/agregarBDUsuarioMaster

//Parametros 
{
    "nombres": "Carlos",
    "primerApellido": "Billagran",
    "segundoApellido": "Roblez",
    "ci": "5689124",
    "telefono": "78562312",
    "idUsuarioVerificar": "1"
}
//Return

{"nombreUsuario":"GVa256","password":"120453822","mensaje":1}


1.1AgregarDirecto()


//URL
http://trial-bin.000webhostapp.com/colegio/index.php/director/agregarBDDirector
//Parametros
{
    "nombres": "Luis Fernando",
    "primerApellido": "Fernandez",
    "segundoApellido": "Villarroel",
    "ci": "78895612",
    "telefono": "7845282",
    "idColegio": "1",
    "idUsuarioVerificar": "1"
}
//Return
{"nombreUsuario":"LFe768","password":"ec8536de9","mensaje":1}

2. AgregarSecretaria()

//Url
http://trial-bin.000webhostapp.com/colegio/index.php/secretaria/agregarBDSecretaria
//Parametros
{
	"nombres":"Juan",
	"primerApellido":"Perez",	
	"segundoApellido":"Perez",
	"ci":"32456",
	"telefono":"78965623",		
	"idColegio"	:"1",
	"idUsuarioVerificar":"1"
}
//Return
{"nombreUsuario":"MRi697","password":"0c341f327","mensaje":1}

3. AgregarTutor()

//URL
http://trial-bin.000webhostapp.com/colegio/index.php/tutor/agregarBDTutor
//Parametros
{
    "nombres": "Victor",
    "primerApellido": "Apaza",
    "segundoApellido": "Mercado",
    "ci": "5689234",
    "telefono": "78562312",
    "idColegio": "1",
    "idUsuarioVerificar": "1"
}
//Return
{"nombreUsuario":"VAp625","password":"ac66ccb05","mensaje":1}


4. AgregarEstudiante()


//URL
http://trial-bin.000webhostapp.com/colegio/index.php/estudiante/agregarBDEstudiante
//Parametros
{
    "nombres": "Freddy",
    "primerApellido": "Apaza",
    "segundoApellido": "Lopez",
    "ci": "5689561",
    "telefono": "68594578",
    "idColegio": "1",
    "idUsuarioVerificar": "1",
    "tutor":[13]
}
//Return
{"nombreUsuario":"FAp685","password":"720c9110f","mensaje":1}

5. AgregarMaestro()

//URL
http://trial-bin.000webhostapp.com/colegio/index.php/maestro/agregarBDMaestro
//Parametros
{
    "nombres": "Elena",
    "primerApellido": "Azurduy",
    "segundoApellido": "Padilla",
    "ci": "5689561",
    "telefono": "75458932",
    "idColegio": "1",
    "idUsuarioVerificar": "1"
}
//Return
{"nombreUsuario":"EAz451","password":"720c9110f","mensaje":1}

6. ModificarUsuario()


//URL
http://trial-bin.000webhostapp.com/colegio/index.php/usuario/modificarUsuarioBd
//Parametros
{
	"nombres":"Victor",
	"primerApellido":"Apaza",
	"segundoApellido":"Botitano",
	"ci":"5689234",
	"telefono":"78562312",
	"idUsuario":"13"
}
//Return
{"mensaje":1}

7.8. Habilitar/InhabilitarUsuario()

//Habilitar
//URL
http://trial-bin.000webhostapp.com/colegio/index.php/usuario/usuarioHabilitar
//Parametros
{
	"idUsuario":"13",
	"rol":"5"
}
//Return
{"mensaje":1}

//Inhabilitar
//URL
http://trial-bin.000webhostapp.com/colegio/index.php/usuario/usuarioDeshabilitar
//Parametros
{
"idUsuario":"13",
"rol":"5"
}
//Return
{"mensaje":1}


9. BuscarUsuariosColegio()

//URL
http://trial-bin.000webhostapp.com/colegio/index.php/usuario/buscarUsuario
//Parametros
{
"busqueda":"i",
"tipoUsuario":"2",
"idColegio":"1"
}
//Return
[{"idUsuario":"1","nombre":"Aparicio Estrada Elian","idColegio":"1"}]

9.1 BuscarGetColegio()
//URL
http://trial-bin.000webhostapp.com/colegio/index.php/usuario/buscarGetUsuario
//Parametros
{
"busqueda":"i",
"tipoUsuario":"2",
"idColegio":"1"
}
//Return
[{"idUsuario":"1","primerApellido":"Aparicio","segundoApellido":"Estrada","nombres":"Elian","ci":"6543562","telefono":"78582684","nombreUsuario":"EA218"}]

10. CambioContrasenia()


//URL
http://trial-bin.000webhostapp.com/colegio/index.php/usuario/cambiarContrasena
//Parametros
{
	"antiguaContrasena":"b1b2664383ed1af776ab76ad96595f22",
	"nuevaContrasena":"847e48f63b1ce31e99e99f7e002574b5",
	"idUsuario":"2"
}
//Return
{"mensaje":1};
10.1 GetSuperAdmin()
//URL
http://trial-bin.000webhostapp.com/colegio/index.php/usuariomaster/getSuperAdmin
//Parametros

//Return
{"idUsuarioMaster":"1","primerApellido":"Valdez","segundoApellido":"Perez","nombres":"Ramon","ci":"5689457","telefono":"68562345","nombreUsuario":"adminMaster"}

10.2 ModificarUsuarioMaster()
//URL
http://trial-bin.000webhostapp.com/colegio/index.php/usuariomaster/modificarUsuarioMaster
//Parametros
{
	"nombres":"Ramons",
	"primerApellido":"Valdez",
	"segundoApellido":"Perezs",
	"ci":"5689231",
	"telefono":"78965623",
	"idUsuario":"1"
}
//Return
{"mensaje":1}
#endregion

#region Colegio
 GetColegio()//Retorna todos los colegios
//URL
http://trial-bin.000webhostapp.com/colegio/index.php/colegio/getColegios
//Parametros
//Return
[{"idColegio":"1","nombreColegio":"Cristinas Prados","estado":"1"},{"idColegio":"2","nombreColegio":"Jose Melchor Cuadros","estado":"1"}]

11. AgregarColegio()


//URL
http://trial-bin.000webhostapp.com/colegio/index.php/usuariomaster/agregarBDColegioDirector
//Parametros
{
    "nombreColegio": "Juancito Pinto",
    "nombres": "Miguel",
    "primerApellido": "Angel",
    "segundoApellido": "Perez",
    "ci": "5689561",
    "telefono": "72569878",
    "idUsuarioVerificar": "1"
}
//Return
{"nombreUsuario":"MA264","password":"720c9110f","mensaje":1}

12. ModificarColegio()

//URL
http://trial-bin.000webhostapp.com/colegio/index.php/colegio/modificarColegio
//Parametros
{
	"idColegio":"3",
	"nombreColegio":"Juancitos Pintos",
	"idUsuarioVerificar":"1"
}
//Return
{"mensaje":1}

13.14. Habilitar/InhabilitarColegio()

//Inhabilitar
//URL
http://trial-bin.000webhostapp.com/colegio/index.php/colegio/colegioDeshabilitar
//Parametros
{
    "idColegio": "1"
  
}
//Return
{"mensaje":1}
//Habilitar

//URL
http://trial-bin.000webhostapp.com/colegio/index.php/colegio/colegioHabilitar
//Parametros
{
    "idColegio": "1"
}
//Return
{"mensaje":1}

15. GetGestiones()

//URL
http://trial-bin.000webhostapp.com/colegio/index.php/gestion/getGestionesColegio
//Parametros
{
"idColegio":"1"
}
//Return
[{"idGestion":"1","anio":"2019","estado":"1"},{"idGestion":"2","anio":"2018","estado":"0"}]

16. AgregarGestiónActiva()
//URL
http://trial-bin.000webhostapp.com/colegio/index.php/gestion/agregarGestionActiva
//Parametros
{
	"idColegio":"1",
	"anio":"2017"
}
//Return
{"mensaje":3}

17. FinalizarGestionActiva()


//URL
http://trial-bin.000webhostapp.com/colegio/index.php/gestion/FinalizarGestionActiva
//Parametros
{
	"idGestion":"2"
}
//Return
{"mensaje":1}


#endregion


#region Cursos

 AgregarCurso()18.

//URL
http://trial-bin.000webhostapp.com/colegio/index.php/curso/agregarCurso
//Parametros
{
	"idColegio":"1",
	"grado":"3",
	"nivel":"Primaria",
	"paralelo":"A"
}
//Return
{"mensaje":1}

19. GetCursosColegio()

//URL
http://trial-bin.000webhostapp.com/colegio/index.php/curso/getCursoColegio
//Parametros
{
	"idColegio":"1"
}
[{"idCurso":"1","idColegio":"1","nivel":"Primaria","grado":"1","paralelo":"A"},{"idCurso":"2","idColegio":"1","nivel":"Primaria","grado":"2","paralelo":"A"},{"idCurso":"3","idColegio":"1","nivel":"Primaria","grado":"3","paralelo":"A"}]


20. EliminarCurso()

//URL
http://trial-bin.000webhostapp.com/colegio/index.php/curso/eliminarCurso
//Parametros
{
	"idCurso":"1"
}
//Return
{"mensaje":1}


ModificarCurso()
//URL
http://trial-bin.000webhostapp.com/colegio/index.php/curso/modificarCurso
//Parametros
{
	"idCurso":"1",
	"grado":"1",
	"nivel":"Primaria",
	"paralelo":"B"
}
//Return
{"mensaje":1}

21. GetEstudiantesCursoGestionActual()


//URL

//Parametros
//Return


#endregion




#region  Materia
 AgregarMateria()22.

//URL
http://trial-bin.000webhostapp.com/colegio/index.php/materia/agregarMateria
//Parametros
{
	"idCurso":"1",
	"idProfesor":"11",
	"idColegio":"1",
	"nombre":"Ciencias Naturales"
}
//Return
{"mensaje":1}

23.24. Habilitar/InhabilitarMateria()
//Inhabilitar
//URL
http://trial-bin.000webhostapp.com/colegio/index.php/materia/InhabilitarMateria
//Parametros
{
	"idMateria":"1"
}
//Return
{"mensaje":1}

//Habilitar
//URL
http://trial-bin.000webhostapp.com/colegio/index.php/materia/HabilitarMateria
//Parametros
{
	"idMateria":"1"
}
//Return
{"mensaje":1}

25. GetMateriasColegio()
//URL
http://trial-bin.000webhostapp.com/colegio/index.php/materia/MateriasColegio
//Parametros
{
	"idColegio":"1"
}
//Return
[{"idMateria":"6","nombre":"Ciencias
Naturales"},{"idMateria":"2","nombre":"Lenguaje"},{"idMateria":"1","nombre":"Matematicas"},{"idMateria":"4","nombre":"Sociales"}]

GetMateriasCursoColegio()
//URL
http://trial-bin.000webhostapp.com/colegio/index.php/materia/materiasCursoColegio
//Parametros
{
	"idColegio":"1",
	"idCurso":"1"
}
//Return
[{"idMateria":"1","nombre":"Matematicas"},{"idMateria":"2","nombre":"Lenguaje"},{"idMateria":"5","nombre":"Sociales"},{"idMateria":"6","nombre":"Ciencias
Naturales"}]

#endregion






#region Estudiante
 AsignarTutores() 26.
//URL
http://trial-bin.000webhostapp.com/colegio/index.php/estudiante/agregarTutorEst
//Parametros
{
	"tutores":[3],
	"estudiante":"7"
}
//Return
{"mensaje":1}

27. QuitarTutores()


//URL
http://trial-bin.000webhostapp.com/colegio/index.php/estudiante/eliminarTutorEst
//Parametros
{
	"tutor":"3",
	"estudiante":"7"
}
//Return
{"mensaje":1}

28. AsignarACursoGestion()


//URL

//Parametros
//Return

29. AbandonoEstudiante()


//URL

//Parametros
//Return

29.1 InfoEstudiante()
//URL
http://trial-bin.000webhostapp.com/colegio/index.php/estudiante/infoEstudiante
//Parametros
{
	"usuario":"7"
}
//Return
{"usuario":[{"idUsuario":"7","nombres":"Miguel","primerApellido":"Suarez","segundoApellido":"Juana","ci":"5689451","telefono":"6851245","rol":"4","nombreColegio":"Cristinas
Prado"}],"tutores":[{"idTutor":"5","nombre":"Juana Lopez Maria"}]}
#endregion


#region Tutor
GetEstudiantesTutor() //30
//URL
//Parametros
//Return

31. GetNotasEstudianteGestion()

//URL
//Parametros
//Return





#endregion

#region Horario
GetHorarioEstudiante()//32. 

//URL
//Parametros
//Return

33. GetHorarioCurso()


//URL
//Parametros
//Return

34. AsignarHorarioCurso()


//URL
//Parametros
//Return

35. GetHorarioProfesor()


//URL
//Parametros
//Return
35.1 AgregarPeriodo()

//URL
http://trial-bin.000webhostapp.com/colegio/index.php/periodo/agregarPeriodo
//Parametros
{
	"periodo":[{
		"horaInicio":"06:35",
		"horaFin":"07:35"
	},
	{
		"horaInicio":"07:35",
		"horaFin":"08:35"
	},
	{
		"horaInicio":"08:45",
		"horaFin":"09:45"
	},
	{
		"horaInicio":"09:45",
		"horaFin":"10:45"
	}
	],
	"idColegio":"1",
	"idUsuarioVerificar":1
}
//Return
{"mensaje":1}

35.2 GetPeriodoColegio()

//URL
http://trial-bin.000webhostapp.com/colegio/index.php/periodo/getPeriodosColegio
//Parametros
{
	"idColegio":"1"
}
//Return
{"idPeriodo":"1","horaInicio":"06:35:00","horaFin":"07:35:00"}
35.3 EliminarPeriodo()

//URL
http://trial-bin.000webhostapp.com/colegio/index.php/periodo/eliminarPeriodo
//Parametros
{
	"idPeriodo":"1"
}
//Return
{"mensaje":1}

#endregion

#region Asistencia
GetCursosDiaProfesor()//36
//URL
//Parametros
//Return

37. GetEstudiantesCurso()


//URL
//Parametros
//Return

38. AgregarAsistenciaIndividual()


//URL
//Parametros
//Return

39. GetAsisenciasCursoUltimosNDias()

//URL
//Parametros
//Return

40. GetAsistenciasCursosRangoFechas()
//URL
//Parametros
//Return


//URL
//Parametros
//Return
#endregion


#region hola
#endregion