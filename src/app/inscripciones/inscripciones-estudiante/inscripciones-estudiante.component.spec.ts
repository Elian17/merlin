import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InscripcionesEstudianteComponent } from './inscripciones-estudiante.component';

describe('InscripcionesEstudianteComponent', () => {
  let component: InscripcionesEstudianteComponent;
  let fixture: ComponentFixture<InscripcionesEstudianteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InscripcionesEstudianteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InscripcionesEstudianteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
