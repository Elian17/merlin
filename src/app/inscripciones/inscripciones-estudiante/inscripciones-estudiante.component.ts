import { Component, OnInit } from '@angular/core';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { SessionStorageService } from 'ngx-webstorage';
import { CursosService } from 'src/app/servicios/cursos.service';
import { InscripcionesService } from 'src/app/servicios/inscripciones.service';
import { NuevainscripcionComponent } from '../nuevainscripcion/nuevainscripcion.component';

export interface DialogDataInscripcion {

  estudiante:Array<any>;
  idCurso: number;


}
@Component({
  selector: 'app-inscripciones-estudiante',
  templateUrl: './inscripciones-estudiante.component.html',
  styleUrls: ['./inscripciones-estudiante.component.scss']
})
export class InscripcionesEstudianteComponent implements OnInit {

vNivel:any;
vCurso:any;

Niveles:any;
Estudiante:any;
idCursoSeleccionado;
Curso:any;

idCurso:number;
idEstudiante:Array<any>=[];

  displayedColumns: string[] = ['Estudiante', 'CI', 'Telefono','Estado'];
dataSource = this.Estudiante;
  constructor(public dialog: MatDialog,private sessionStorage:SessionStorageService,private inscripcionServicio:InscripcionesService,private cursoService:CursosService) {

  }

  async ngOnInit() {
    this.Niveles=await this.cursoService.GetNivelesCurso(parseInt(sessionStorage.getItem('colegio')));

  }
  selectedNivels(value:any):void {
    console.log('Selected value is:sdsda ', value);
    this.Curso=null;
    if(value !="undefined"){
      this.getcursoNivel(value);

    }
  }
    async getcursoNivel(nivel:string){
      this.Curso=await this.cursoService.GetCursoNivel(parseInt(sessionStorage.getItem('colegio')),nivel);
      console.log(this.Curso);
    }
    public selectedCurso(value:any):void {
      console.log('change ', value);

        this.idCursoSeleccionado=value.idCurso
        this.selectEstudianteCurso(value.idCurso);


    }
    async selectEstudianteCurso(idCurs:number){
      if(idCurs!=undefined){
        this.Estudiante=await this.inscripcionServicio.GetCursoEstudiante(parseInt(sessionStorage.getItem('colegio')),idCurs);
        this.dataSource = new MatTableDataSource(this.Estudiante);
      }
     }
     CambioEstado(idCursoEstudiante:number,estado:number){
       console.log(idCursoEstudiante+ "     dwadaw");
      if(estado==1){
        this.inscripcionServicio.InhabilitarInscripcionEstudiante(idCursoEstudiante);
      }
      else{
        this.inscripcionServicio.HabilitarInscripcionEstudiante(idCursoEstudiante);
      }
      this.selectEstudianteCurso(this.idCursoSeleccionado);
      }
      openDialog(): void {
        const dialogRef = this.dialog.open(NuevainscripcionComponent, {
          width: '500px',
          data: { estudiante:this.idEstudiante, idCurso: this.idCurso,
                  }
        });

        dialogRef.afterClosed().subscribe(async result => {
        await this.agregarInscripcion(result.estudiante,
                                                result.idCurso,
                                                parseInt(sessionStorage.getItem('colegio')

          ));

        this.selectEstudianteCurso(this.idCursoSeleccionado);

        });
      }
   agregarInscripcion(estudiante:number[],idCurso:number,idColegio:number)
    {
      console.log(estudiante);
       console.log('inscripcion ' + idCurso + " " +idColegio);
      // console.log(materia);
        this.inscripcionServicio.AgregarInscripcionEstudiante(estudiante,idCurso,idColegio);
    }
}
