import { Component, OnInit, Inject } from '@angular/core';
import { DialogDataInscripcion } from '../inscripciones-estudiante/inscripciones-estudiante.component';
import { SessionStorageService } from 'ngx-webstorage';
import { UsuarioService } from 'src/app/servicios/usuarios.service';
import { CursosService } from 'src/app/servicios/cursos.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-nuevainscripcion',
  templateUrl: './nuevainscripcion.component.html',
  styleUrls: ['./nuevainscripcion.component.scss']
})
export class NuevainscripcionComponent implements OnInit {

  Curso:any;
Estudiante:any;
Niveles:any;
nombreEstudiante:string;
idEstudiante:number;
idCurso:number;
estudiantesAgregados: Array<string> = [];
  constructor(private usuarioService:UsuarioService,private cursoService:CursosService,private ssionStorage:SessionStorageService,public dialogRef:MatDialogRef<NuevainscripcionComponent>,
    @Inject(MAT_DIALOG_DATA) public data:DialogDataInscripcion) { }

  async ngOnInit() {
    this.Niveles=await this.cursoService.GetNivelesCurso(parseInt(sessionStorage.getItem('colegio')));
    this.Estudiante =await this.usuarioService.BuscarUsuariosColegio('-1',4,parseInt(sessionStorage.getItem('colegio')))
    console.log(this.Estudiante);
    console.log("ahoracargar datos");
  }
  selectedNivels(value:any):void {
    console.log('Selected value is:sdsda ', value);
    this.Curso=null;
    if(value !="undefined"){
      this.getcursoNivel(value.nivel);
      
  }
}
selectEstudiante(value:any):void {
  console.log('Selected value is:sdsda ', value);
    this.idEstudiante = value.idUsuario;
    this.nombreEstudiante = value.nombre;
}
  async getcursoNivel(nivel:string){
    this.Curso=await this.cursoService.GetCursoNivel(parseInt(sessionStorage.getItem('colegio')),nivel);
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  agregarEstudiante(){
    var duplicado=false;
    this.data.estudiante.forEach(objeto => {
      if(objeto.idEstudiante==this.idEstudiante)
      {
        duplicado=true;
      }

    });
    if(!duplicado)
    {
      if(this.idEstudiante!=null)
      {
        let data={
          "idEstudiante":this.idEstudiante,
          "nombre":this.nombreEstudiante
        }
        this.data.estudiante.push(data);
      }
    }
    console.log(this.data.estudiante);
  }
  quitarEstudiante(idEstudiante:number)
  {
    this.data.estudiante.forEach(objeto => {
      if(objeto.idEstudiante==idEstudiante)
      {
        this.data.estudiante.splice(this.data.estudiante.indexOf(objeto), 1);
      }

    });

    console.log(this.data.estudiante);
  }

}
