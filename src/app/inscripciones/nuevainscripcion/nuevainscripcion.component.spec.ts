import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevainscripcionComponent } from './nuevainscripcion.component';

describe('NuevainscripcionComponent', () => {
  let component: NuevainscripcionComponent;
  let fixture: ComponentFixture<NuevainscripcionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevainscripcionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevainscripcionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
