import { Constantes } from './../../Clases/constantes';
import { NuevoUsuarioComponent } from './../nuevo-usuario/nuevo-usuario.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { UsuarioService } from 'src/app/servicios/usuarios.service';
import { MatSort, MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';
import { Usuario } from 'src/app/Clases/usuario';
import { Rol } from 'src/app/Clases/rol';

@Component({
  selector: 'app-usuariosColegio',
  templateUrl: './usuariosColegio.component.html',
  styleUrls: ['./usuariosColegio.component.scss']
})
export class UsuariosColegioComponent implements OnInit {

  idUsuario;
  busqueda: string;
  Usuarios: any;

  nombreColegio: string;
  nombres: string;
  primerApellido: string;
  segundoApellido: string;
  ci: string;
  telefono: string;
  rol: number;
  tutores: any[];

  displayedColumns: string[] = ['nombreUsuario', 'nombre', 'telefono', 'rol', 'Estado', 'Editar'];
  dataSource = this.Usuarios;

  roles: Rol[]=null;
  rolSeleccion;


  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort,{static: false}) sort: MatSort;

  constructor(public dialog: MatDialog,
              private usuarioService: UsuarioService) { }

  ngOnInit() {
    this.idUsuario=sessionStorage.getItem('user');
    this.CargarRoles();

  }

  async Buscar(text: string){
    this.busqueda=text;
    this.Usuarios =
    await this.usuarioService.BuscarUsuariosColegio(this.busqueda,
                                                    this.rolSeleccion,
                                                    parseInt(sessionStorage.getItem('colegio')));
    this.dataSource = new MatTableDataSource(this.Usuarios);
    console.log(this.Usuarios);

  }
  CargarRoles(){
    this.roles=Constantes.Roles;
    if (this.roles==null) {
      this.CargarRoles();
    }
    else{
    }
  }


  openDialog(): void {
    var user;
    const dialogRef = this.dialog.open(NuevoUsuarioComponent, {
      width: '500px',
      data: { nuevo: true, nombres: this.nombres, primerApellido: this.primerApellido,
              segundoApellido: this.segundoApellido, ci: this.ci, telefono: this.telefono,
              rol: this.rol, tutores: []}
    });

    dialogRef.afterClosed().subscribe(async result => {
      console.log(result);
      switch (result.rol) {
        case Constantes.Director:
          user = await this.usuarioService.AgregarDirector(
            new Usuario(0, result.nombres, result.primerApellido, result.segundoApellido, result.ci, result.telefono),
            parseInt(sessionStorage.getItem('colegio')));
          break;
        case Constantes.Administrativo:
          user = await this.usuarioService.AgregarSecretaria(
            new Usuario(0, result.nombres, result.primerApellido, result.segundoApellido, result.ci, result.telefono),
            parseInt(sessionStorage.getItem('colegio')));
          break;
        case Constantes.Tutor:
          user = await this.usuarioService.AgregarTutor(
            new Usuario(0, result.nombres, result.primerApellido, result.segundoApellido, result.ci, result.telefono),
            parseInt(sessionStorage.getItem('colegio')));
          break;
        case Constantes.Estudiante:
          var tutoresEstudiante=[];
          result.tutores.forEach(tutor => {
            tutoresEstudiante.push(tutor.idUsuario);
          });
          console.log(tutoresEstudiante);
          user = await this.usuarioService.AgregarEstudiante(
            new Usuario(0, result.nombres, result.primerApellido, result.segundoApellido, result.ci, result.telefono),
            parseInt(sessionStorage.getItem('colegio')), tutoresEstudiante);
          break;
        case Constantes.Maestro:
          user = await this.usuarioService.AgregarMaestro(
            new Usuario(0, result.nombres, result.primerApellido, result.segundoApellido, result.ci, result.telefono),
            parseInt(sessionStorage.getItem('colegio')));
          break;
        default:
          alert("Debe Seleccionar un rol");
          break;
      }
      console.log(user);
      this.Buscar(this.busqueda);
    });
  }

  editar(user: any) {
    var dialogRef;

    if(user.rol==4){
      dialogRef = this.dialog.open(NuevoUsuarioComponent, {
        width: '500px',
        data: { nuevo: false, idUsuario: user.idUsuario, nombres: user.nombres, primerApellido: user.primerApellido,
                segundoApellido: user.segundoApellido, ci: user.ci, telefono: user.telefono, rol: user.rol, tutores: user.tutores}
      });
      dialogRef.afterClosed().subscribe(async result => {
        await this.usuarioService.ModificarUsuario(new Usuario(result.idUsuario, result.nombres, result.primerApellido, result.segundoApellido, result.ci, result.telefono));
      });
    }
    else{
      dialogRef = this.dialog.open(NuevoUsuarioComponent, {
        width: '500px',
        data: { nuevo: false, idUsuario: user.idUsuario, nombres: user.nombres, primerApellido: user.primerApellido,
                segundoApellido: user.segundoApellido, ci: user.ci, telefono: user.telefono, rol: user.rol}
      });
      dialogRef.afterClosed().subscribe(async result => {
        await this.usuarioService.ModificarUsuario(new Usuario(result.idUsuario, result.nombres, result.primerApellido, result.segundoApellido, result.ci, result.telefono));
      });
    }
    this.Buscar(this.busqueda);

  }
  async Habilitar(idUsuario: number){
    await this.usuarioService.HabilitarUsuario(idUsuario);
    this.Buscar(this.busqueda);
  }

  async Inhabilitar(idUsuario: number){
    switch (await this.usuarioService.InhabilitarUsuario(idUsuario)) {
      case 1:
        alert("Usuario deshabilitado");
        this.Buscar(this.busqueda);
        break;
      case 2:
        alert("No tiene los permisos suficientes para realizar esta acción");
        break;
      default:
        break;
    }
  }

}
