import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsuariosColegioComponent } from './usuariosColegio.component';

describe('UsuariosComponent', () => {
  let component: UsuariosColegioComponent;
  let fixture: ComponentFixture<UsuariosColegioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsuariosColegioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsuariosColegioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
