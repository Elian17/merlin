import { UsuarioService } from 'src/app/servicios/usuarios.service';
import { Constantes } from 'src/app/Clases/constantes';
import { Component, OnInit, Inject} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogData } from 'src/app/usuarios/admin-inicio/admin-inicio.component';
import { Usuario } from 'src/app/Clases/usuario';
import { Rol } from 'src/app/Clases/rol';

@Component({
  selector: 'app-nuevo-usuario',
  templateUrl: './nuevo-usuario.component.html',
  styleUrls: ['./nuevo-usuario.component.scss']
})
export class NuevoUsuarioComponent implements OnInit {

  nuevo: boolean;
  nombres: string;
  primerApellido: string;
  segundoApellido: string;
  ci: string;
  telefono: string;
  rol: number;
  tutores: any[]=[];

  roles: Rol[] = null;
  usuarios: any[];



  constructor(public dialogRef: MatDialogRef<NuevoUsuarioComponent>,
              public usuarioService: UsuarioService,
              @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  ngOnInit() {
    this.CargarRoles();
  }
  CargarRoles() {
    this.roles = Constantes.Roles;
    if (this.roles == null) {
      this.CargarRoles();
    } else {
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  BuscarTutores(texto: string) {
    // if (texto.length>3) {

    // }
    this.tutores = [];
    this.usuarioService.BuscarUsuariosColegioNombreCompleto(texto,
      Constantes.Tutor,
      parseInt(sessionStorage.getItem('colegio'))).then(res => this.tutores=res as any[]);
  }
  Asignar(tutor: any){
    var repetido;
    this.data.tutores.forEach(tutorSelect => {
      if (tutorSelect.idUsuario==tutor.idUsuario) {
        repetido=true;
      }
    });
    if (!repetido) {
      this.data.tutores.push(tutor);
    }
  }
  Quitar(tutor: any){
    var cont=0;
    this.data.tutores.forEach(tutorSelect => {
      if (tutorSelect.idUsuario==tutor.idUsuario) {
        this.data.tutores.splice(cont, 1);
      }
      cont++;

    });
  }

}
