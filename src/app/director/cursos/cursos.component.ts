import { Component, OnInit, OnChanges } from '@angular/core';
import { CursosService } from 'src/app/servicios/cursos.service';
import { Sesion } from 'src/app/Clases/sesion';
import { SessionStorageService } from 'ngx-webstorage';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { Curso } from 'src/app/Clases/curso';
import { NuevoCursoComponent } from '../nuevo-curso/nuevo-curso.component';

@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.scss']
})
export class CursosComponent implements OnInit {
o:number;
  grado:number;
  nivel:string;
  paralelo:string;
  Cursos:any;
  displayedColumns: string[] = ['Grado', 'Paralelo', 'Nivel','Eliminar','Editar'];
  dataSource = this.Cursos;
  constructor(private cursoService:CursosService,private sessionStorage:SessionStorageService,public dialog: MatDialog) { }

  ngOnInit() {
    this.recargarCursos();
  }
  async recargarCursos(){
    this.Cursos =await this.cursoService.GetCursosColegio(parseInt(sessionStorage.getItem('colegio')));
    console.log(this.Cursos);
    console.log(sessionStorage.getItem('colegio'));
    this.dataSource = new MatTableDataSource(this.Cursos);
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(NuevoCursoComponent, {
      width: '500px',
      data: { grado: this.grado, nivel: this.nivel, paralelo: this.paralelo
              }
    });

    dialogRef.afterClosed().subscribe(async result => {
    await this.agregarCurso(new Curso(
                                            parseInt(sessionStorage.getItem('colegio')), result.grado,
                                            result.nivel,
                                            result.paralelo,1
                                            
                                            
      ));
    this.recargarCursos();

    });
  }
  async agregarCurso(curso: Curso ){
    await this.cursoService.AgregarCursoColegio(curso);
  }

  OpenDialogEditarCurso(idCurso:number,CursoS:any): void {
    const dialogRef = this.dialog.open(NuevoCursoComponent, {
      width: '500px',
      data: { grado: CursoS.grado, nivel: CursoS.nivel, paralelo: CursoS.paralelo
              }
    });

    dialogRef.afterClosed().subscribe(async result => {
    await this.modificarCurso(idCurso,new Curso(
                                            0, result.grado,
                                            result.nivel,
                                            result.paralelo,1
                                            
                                            
      ));
    this.recargarCursos();

    });
  }
  async modificarCurso(idCurso:number,curso: Curso ){
    await this.cursoService.ModificarCursoColegio(idCurso,curso);
  }
  eliminarCurso(idCurso:number){
    this.cursoService.EliminarCurso(idCurso);
    this.recargarCursos();
  }
}
