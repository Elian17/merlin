import { Component, OnInit, Inject } from '@angular/core';
import { DialogData } from 'src/app/usuarios/admin-inicio/admin-inicio.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-nuevo-curso',
  templateUrl: './nuevo-curso.component.html',
  styleUrls: ['./nuevo-curso.component.scss']
})
export class NuevoCursoComponent implements OnInit {
  idCurso:number;
  grado:number;
  nivel:string;
  paralelo:string;
  constructor( public dialogRef:MatDialogRef<NuevoCursoComponent>,
    @Inject(MAT_DIALOG_DATA) public data:DialogData,) { }

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
