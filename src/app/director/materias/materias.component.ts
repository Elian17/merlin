import { Component, OnInit } from '@angular/core';
import { CursosService } from 'src/app/servicios/cursos.service';
import { SessionStorageService } from 'ngx-webstorage';
import { MateriaService } from 'src/app/servicios/materia.service';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { NuevaMateriaComponent } from '../nueva-materia/nueva-materia.component';
import { Materia } from 'src/app/Clases/materia';
export interface DialogData1 {

  materia:Array<Materia>;
  idCurso: number;
  

}
@Component({
  selector: 'app-materias',
  templateUrl: './materias.component.html',
  styleUrls: ['./materias.component.scss']
})

export class MateriasComponent implements OnInit {

  vNivel:any;
  vCurso:any;
Niveles:any;
Curso:any;
nombreMateria:string;
idCurso:number;
idProfesor:number;
materiaAgregadas:Array<Materia>=[];

 idCursoSeleccionado:number;
Materia:any;
displayedColumns: string[] = ['Materia', 'Docente', 'Estado','Editar'];
dataSource = this.Materia;
  constructor(public dialog: MatDialog,private cursoService:CursosService,private materiaService:MateriaService,private sessionStorage:SessionStorageService) { }

  async ngOnInit() {
   this.Niveles=await this.cursoService.GetNivelesCurso(parseInt(sessionStorage.getItem('colegio')));
  
   console.log(this.Niveles);
   console.log(this.Curso);
 ;
   console.log("nivels");
  }
   selectedNivels(value:any):void {
    console.log('Selected value is:sdsda ', value);
    this.Curso=null;
    if(value !="undefined"){
      this.getcursoNivel(value);
    
    }
   
  }
  async getcursoNivel(nivel:string){
    this.Curso=await this.cursoService.GetCursoNivel(parseInt(sessionStorage.getItem('colegio')),nivel);
  }
  public selectedCurso(value:any):void {
    console.log('change ', value);
    
      this.idCursoSeleccionado=value.idCurso
      this.selectMateriaProfesor(value);
    
    
  }
 async selectMateriaProfesor(idCurs:number){
  if(idCurs!=undefined){
    this.Materia=await this.materiaService.GetMateriasProfesor(parseInt(sessionStorage.getItem('colegio')),idCurs);
    this.dataSource = new MatTableDataSource(this.Materia);
  }
 }
 openDialog(): void {
  const dialogRef = this.dialog.open(NuevaMateriaComponent, {
    width: '500px',
    data: { materia:this.materiaAgregadas, idCurso: this.idCurso,
            }
  });

  dialogRef.afterClosed().subscribe(async result => {
  await this.agregarMateria(result.materia,
                                          parseInt(sessionStorage.getItem('colegio')
                                         
    ));
    
  this.selectMateriaProfesor(this.idCursoSeleccionado);

  });
}

agregarMateria(materia:Materia[],idcolegio:number)
{
  console.log('materia');
  console.log(materia);
  this.materiaService.AgregarMateria(materia,idcolegio);
}

CambioEstado(idMateria:number,estado:number){
if(estado==1){
  this.materiaService.InhabilitarMateria(idMateria);
}
else{
  this.materiaService.HabilitarMateria(idMateria);
}
this.selectMateriaProfesor(this.idCursoSeleccionado);
}

}
