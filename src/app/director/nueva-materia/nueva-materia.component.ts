import { Component, OnInit, Inject } from '@angular/core';
import { CursosService } from 'src/app/servicios/cursos.service';
import { SessionStorageService } from 'ngx-webstorage';

import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { UsuarioService } from 'src/app/servicios/usuarios.service';
import { Materia } from 'src/app/Clases/materia';
import { DialogData1 } from '../materias/materias.component';

@Component({
  selector: 'app-nueva-materia',
  templateUrl: './nueva-materia.component.html',
  styleUrls: ['./nueva-materia.component.scss']
})


export class NuevaMateriaComponent implements OnInit {

  vNivel:any;
  vCurso:any;

Curso:any;
Profesor:any;
Niveles:any;
//materia:Array<Materia>=[];
nombreMateria:string;
docente=15;
idCurso:number;
materiasAgregadas: Array<Materia> = [];


  constructor(private usuarioService:UsuarioService,private cursoService:CursosService,private ssionStorage:SessionStorageService,public dialogRef:MatDialogRef<NuevaMateriaComponent>,
    @Inject(MAT_DIALOG_DATA) public data:DialogData1) { 
      this.data.materia =[];
    
    }

  async ngOnInit() {
    this.Niveles=await this.cursoService.GetNivelesCurso(parseInt(sessionStorage.getItem('colegio')));
    this.Profesor =await this.usuarioService.BuscarUsuariosColegio('-1',6,parseInt(sessionStorage.getItem('colegio')))
    console.log(this.Profesor);
    console.log("ahoracargar datos");
   // this.nombreMateria = "dwdw";
   // this.docente =15;
  }
  selectedNivels(value:any):void {
    console.log('Selected value is:sdsda ', value);
    this.Curso=null;
    if(value !="undefined"){
      this.getcursoNivel(value);
      
  }
   
  }
  async getcursoNivel(nivel:string){
    this.Curso=await this.cursoService.GetCursoNivel(parseInt(sessionStorage.getItem('colegio')),nivel);
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  agregarMateria(){
  
    var duplicado=false;
    this.data.materia.forEach(objeto => {
      if(objeto.Nombre==this.nombreMateria)
      {
        duplicado=true;
      }

    });
    if(!duplicado)
    {
      if(this.nombreMateria!=null)
      {
      
        this.data.materia.push(new Materia(0,this.nombreMateria,0,this.idCurso,this.docente,0))
      }
    }
    console.log(this.data.materia);

  }
  quitarMateria(nombre:string)
  {
    this.data.materia.forEach(objeto => {
      if(objeto.Nombre==nombre)
      {
        this.data.materia.splice(this.data.materia.indexOf(objeto), 1);
      }

    });

    console.log(this.data.materia);
  }


}
