import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { DialogDataComunicado } from '../comunicado/comunicado.component';

@Component({
  selector: 'app-vercomunicado',
  templateUrl: './vercomunicado.component.html',
  styleUrls: ['./vercomunicado.component.scss']
})
export class VercomunicadoComponent implements OnInit {

  constructor(public dialogRef:MatDialogRef<VercomunicadoComponent>,
    @Inject(MAT_DIALOG_DATA) public data:DialogDataComunicado,) { }

  ngOnInit() {
    console.log(this.data);
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
 
}
