import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VercomunicadoComponent } from './vercomunicado.component';

describe('VercomunicadoComponent', () => {
  let component: VercomunicadoComponent;
  let fixture: ComponentFixture<VercomunicadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VercomunicadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VercomunicadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
