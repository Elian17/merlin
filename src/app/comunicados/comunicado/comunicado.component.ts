import { Component, OnInit, ViewChild } from '@angular/core';
import { ComunicadosService } from 'src/app/servicios/comunicados.service';
import { MatTableDataSource, MatDialog, MatPaginator, MatSort } from '@angular/material';
import { SessionStorageService } from 'ngx-webstorage';
import { CrearComunicadoComponent } from '../crear-comunicado/crear-comunicado.component';
import { UpdateComunicadoComponent } from '../update-comunicado/update-comunicado.component';
import { VercomunicadoComponent } from '../vercomunicado/vercomunicado.component';
import { WebSocketService } from 'src/app/servicios/web-socket.service';
export interface DialogDataComunicado {
  titulo: string;

  mensaje: string;
  tipo: string;
  tipoAgregar: Array<any>;
  idCurso: number;


  Receptores: Array<any>;

}
@Component({
  selector: 'app-comunicado',
  templateUrl: './comunicado.component.html',
  styleUrls: ['./comunicado.component.scss']
})
export class ComunicadoComponent implements OnInit {
//Datos
  titulo: string;
  mensaje: string;
  tipo: string;
  tipoAgregar: number;
  idCurso: number;
  Receptores: any[];
//Termina datos


Comunicados:any;
  displayedColumns: string[] = ['Titulo','Fecha', 'Ver', 'Eliminar','Editar'];
  dataSource = this.Comunicados;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort,{static: false}) sort: MatSort;
  constructor(public dialog: MatDialog,private comunicadoService:ComunicadosService,private sessionStorage:SessionStorageService,private webSocket:WebSocketService) { }

  ngOnInit() {
    this.recargarComunicados();
  }
  async recargarComunicados() {
    this.Comunicados =await this.comunicadoService.GetComunicados(parseInt(sessionStorage.getItem('user')),parseInt(sessionStorage.getItem('colegio')));

    this.dataSource = new MatTableDataSource(this.Comunicados);
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(CrearComunicadoComponent, {
      width: '500px',
      data: { titulo: this.titulo, mensaje: this.mensaje, tipo: this.tipo,
              tipoAgregar: this.tipoAgregar, idCurso: this.idCurso, Receptores: this.Receptores}
    });

    dialogRef.afterClosed().subscribe(async result => {
      
    await this.agregarComunicados(parseInt(sessionStorage.getItem('user')),
                                    parseInt(sessionStorage.getItem('colegio')),
                                            result.titulo,
                                            result.mensaje,
                                            result.tipo,
                                            result.tipoAgregar,
                                            result.idCurso
      , result.Receptores);
    this.recargarComunicados();
      await this.enviarEvento();
    });

  }
  async enviarEvento(){
    var payload ={
      autor:"juan",
      mensaje: "HOlta wdqdq"
  
    };
    this.webSocket.emit('agregarMensaje',payload);
  }
  agregarComunicados(idUsuario:number,idColegio:number,  titulo: string,mensaje: string,tipo: string, tipoAgregar: [],
    idCurso: number,Receptores: []
  ){
    //console.log(idUsuario + " ");
    //console.log(Receptores);
      this.comunicadoService.AgregarComunicado(idUsuario,idColegio,titulo,mensaje,tipo,tipoAgregar,idCurso,Receptores);
    }
    CambioEstado(idComunicado:number){
      this.comunicadoService.EliminarComunicado(idComunicado);
      this.recargarComunicados();
    }
    OpenDialogEditar(idComunicado:number,titulo:string,mensaje:string,tipo:string): void{
      const dialogRef = this.dialog.open(UpdateComunicadoComponent, {
        width: '500px',
        data: { titulo: titulo, mensaje: mensaje, tipo: tipo,
                }
      });
  
      dialogRef.afterClosed().subscribe(async result => {
      await this.updateComunicado(idComunicado,
                                              result.titulo,
                                              result.mensaje,
                                              result.tipo,
        );
      this.recargarComunicados();
  
      });
    }
    updateComunicado(idComunicado:number,titulo:string,mensaje:string,tipo:string){
      this.comunicadoService.ModificarComunicado(idComunicado,titulo,mensaje,tipo);
    }
    VerDetalles(idComunicado:number,titulo:string,mensaje:string,tipo:string):void{
      const dialogRef = this.dialog.open(VercomunicadoComponent, {
        width: '500px',
        data: { titulo: titulo, mensaje: mensaje, tipo: tipo,
                }
      });
    }

}
