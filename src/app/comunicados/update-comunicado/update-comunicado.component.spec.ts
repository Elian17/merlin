import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateComunicadoComponent } from './update-comunicado.component';

describe('UpdateComunicadoComponent', () => {
  let component: UpdateComunicadoComponent;
  let fixture: ComponentFixture<UpdateComunicadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateComunicadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateComunicadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
