import { Component, OnInit, Inject } from '@angular/core';
import { DialogDataComunicado } from '../comunicado/comunicado.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-update-comunicado',
  templateUrl: './update-comunicado.component.html',
  styleUrls: ['./update-comunicado.component.scss']
})
export class UpdateComunicadoComponent implements OnInit {

  constructor(public dialogRef:MatDialogRef<UpdateComunicadoComponent>,
    @Inject(MAT_DIALOG_DATA) public data:DialogDataComunicado,) { }

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
