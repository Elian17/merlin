import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { DialogDataComunicado } from '../comunicado/comunicado.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CursosService } from 'src/app/servicios/cursos.service';
import { SessionStorageService } from 'ngx-webstorage';
import { UsuarioService } from 'src/app/servicios/usuarios.service';


@Component({
  selector: 'app-crear-comunicado',
  templateUrl: './crear-comunicado.component.html',
  styleUrls: ['./crear-comunicado.component.scss']
})
export class CrearComunicadoComponent implements OnInit {
  Niveles:any;
  Curso:any;

  vNivel:any;
  vCurso:any;
//Docente
nombreDocente:string;
idDocente:string;

rolUsuario:any=[{
  nombre:'Docente',
  tipo:6
},{
  nombre:'Secretaria',
  tipo:3
},{
  nombre:'Estudiante',
  tipo:4
},{
  nombre:'Tutor',
  tipo:5
}];
Profesor:any;
  @ViewChild('item', { static: true }) accordion;
  constructor(public dialogRef:MatDialogRef<CrearComunicadoComponent>,
    @Inject(MAT_DIALOG_DATA) public data:DialogDataComunicado,private cursoService:CursosService,private sessionStorage:SessionStorageService,private usuarioService:UsuarioService) { }

  async ngOnInit() {
    this.Niveles=await this.cursoService.GetNivelesCurso(parseInt(sessionStorage.getItem('colegio')));
    this.data.Receptores=[];
    this.data.tipoAgregar=[];
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  selectedDocente(value:any){
    this.nombreDocente=value.nombre;
    this.idDocente = value.idUsuario;

  }
 async selectedRolDocente(value:any){
    this.Profesor =await this.usuarioService.BuscarUsuariosColegio('-1',value,parseInt(sessionStorage.getItem('colegio')))
  }
  toggle() {
    this.accordion.toggle();
  }
  enviarDocente(){
    this.data.tipoAgregar.push(1);
    console.log(this.data.tipoAgregar);
  }
  enviarCurso(){
    this.data.tipoAgregar.push(2);
    console.log(this.data.tipoAgregar);
  }
  enviarUsuarios(){
    this.data.tipoAgregar.push(3);
    console.log(this.data.tipoAgregar);
  }
  selectedNivels(value:any):void {
    console.log('Selected value is:sdsda ', value);
    this.Curso=null;
    if(value !="undefined"){
      this.getcursoNivel(value);

    }

  }
  async getcursoNivel(nivel:string){
    this.Curso=await this.cursoService.GetCursoNivel(parseInt(sessionStorage.getItem('colegio')),nivel);
  }
  quitarDocente(nombre:string){
    this.data.Receptores.forEach(objeto => {
      if(objeto.nombre==nombre)
      {
        this.data.Receptores.splice(this.data.Receptores.indexOf(objeto), 1);
      }

    });

    console.log(this.data.Receptores);
  }
  agregarDocente(){
    console.log(this.data.Receptores);
    var duplicado=false;
    this.data.Receptores.forEach(objeto=>{
      if(objeto.nombre==this.nombreDocente){
        duplicado=true;
      }
    });
    if(!duplicado){
      if(this.nombreDocente!=null){
       let datos = {
          nombre:this.nombreDocente,
          idDocente:this.idDocente
        }
          this.data.Receptores.push(datos);
      }
    }



  }
}
