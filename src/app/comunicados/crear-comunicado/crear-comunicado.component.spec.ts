import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearComunicadoComponent } from './crear-comunicado.component';

describe('CrearComunicadoComponent', () => {
  let component: CrearComunicadoComponent;
  let fixture: ComponentFixture<CrearComunicadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearComunicadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearComunicadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
