import { Constantes } from 'src/app/Clases/constantes';
import { SessionStorageService } from 'ngx-webstorage';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { NbMenuItem, NbMenuService } from '@nebular/theme';
import { ComunicadosService } from 'src/app/servicios/comunicados.service';
import { VercomunicadoComponent } from 'src/app/comunicados/vercomunicado/vercomunicado.component';
import { MatDialog } from '@angular/material';
import { WebSocketService } from 'src/app/servicios/web-socket.service';

@Component({
  selector: 'main-nav',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.scss']
})
export class MainNavComponent implements OnInit  {
  comunicado:any;
  cantidad:any;
  constructor(public dialog: MatDialog,private menuService: NbMenuService,
              private sessionStorage: SessionStorageService,private comunicadoService:ComunicadosService, private webSocket:WebSocketService) {

                this.cantidad=0;
              }

  Usuario;
  rol;
  RolUsuario:string;

  configuraciones: NbMenuItem[] = [
    {
      title: 'CambioContraseña',
      icon: 'lock-outline',
      link: '/cambioContrasenia',
    }
  ];


  superadmin: NbMenuItem[] = [
    {
      title: 'Usuarios',
      icon: 'people-outline',
      link: '/usuarios',
    },
    {
      title: 'Colegios',
      icon: 'home-outline',
      link: '/admin',
    },
    {
      title: 'Cerrar Sesión',
      icon: 'person-remove-outline',
      link: '/',
    },
    {
      title: 'CambioContraseña',
      icon: 'lock-outline',
      link: '/cambioContrasenia',
    }
  ];


  administrativo: NbMenuItem[] = [
    {
      title: 'Usuarios',
      icon: 'people-outline',
      link: '/usuariosColegio',
    },
    {
      title: 'Comunicados',
      icon: 'message-square-outline',
      link: '/usuariosColegio',
    },
    {
      title: 'Cursos',
      icon: 'grid-outline',
      link: '/cursos',
    },
    {
      title: 'Materias',
      icon: 'book-open-outline',
      link: '/colegios',
    },
    {
      title: 'Horarios',
      icon: 'calendar-outline',
      link: '/colegios',
    },
    {
      title: 'Inscripciones',
      icon: 'clipboard-outline',
      link: '/inscripcionesEstudiante',
    },
    {
      title: 'Reportes',
      icon: 'file-text-outline',
      link: '/reportes',
    },
    {
      title: 'Cerrar Sesión',
      icon: 'person-remove-outline',
      link: '/',
    },
    {
      title: 'CambioContraseña',
      icon: 'lock-outline',
      link: '/cambioContrasenia',
    }
  ];

  director: NbMenuItem[] = [
    {
      title: 'Usuarios',
      icon: 'people-outline',
      link: '/usuariosColegio',
    },
    {
      title: 'Comunicados',
      icon: 'message-square-outline',
      link: '/comunicado',
    },
    {
      title: 'Cursos',
      icon: 'grid-outline',
      link: '/cursos',
    },
    {
      title: 'Materias',
      icon: 'book-open-outline',
      link: '/materia',
    },
    {
      title: 'Periodos',
      icon: 'bookmark-outline',
      link: '/director',
    },
    {
      title: 'Horarios',
      icon: 'calendar-outline',
      link: '/cursoHorario',
    },
    {
      title: 'Inscripciones',
      icon: 'clipboard-outline',
      link: '/inscripcionesEstudiante',
    },
    {
      title: 'Reportes',
      icon: 'file-text-outline',
      link: '/reportes',
    },
    {
      title: 'Cerrar Sesión',
      icon: 'person-remove-outline',
      link: '/',
    },
    {
      title: 'CambioContraseña',
      icon: 'lock-outline',
      link: '/cambioContrasenia',
    }
  ];
  estudiante: NbMenuItem[] = [

    {
      title: 'Asistencias',
      icon: 'person-outline',
      link: '/verAsistenciaEstudiante',
    },
    {
      title: 'Notas',
      icon: 'person-outline',
      link: '/vernotasEstudiante',
    },
    {
      title: 'Cerrar Sesión',
      icon: 'person-remove-outline',
      link: '/',
    },
    {
      title: 'CambioContraseña',
      icon: 'lock-outline',
      link: '/cambioContrasenia',
    }
  ];
  tutor: NbMenuItem[] = [

    {
      title: 'Asistencias',
      icon: 'person-outline',
      link: '/tutorInicio',
    },
    {
      title: 'Notas',
      icon: 'person-outline',
      link: '/vernotasTutor',
    },
    {
      title: 'Cerrar Sesión',
      icon: 'person-remove-outline',
      link: '/',
    },
    {
      title: 'CambioContraseña',
      icon: 'lock-outline',
      link: '/cambioContrasenia',
    }
  ];
  maestro: NbMenuItem[] = [

    {
      title: 'Horario',
      icon: 'calendar-outline',
      link: '/horarios',
    },
    {
      title: 'Asistencias',
      icon: 'clipboard-outline',
      link: '/asistencias',
    },
    {
      title: 'Notas',
      icon: 'layout-outline',
      link: '/notas',
    },
    {
      title: 'Reportes',
      icon: 'file-text-outline',
      link: '/reportes',
    },
    {
      title: 'Cerrar Sesión',
      icon: 'person-remove-outline',
      link: '/',
    },
    {
      title: 'CambioContraseña',
      icon: 'lock-outline',
      link: '/cambioContrasenia',
    }
  ];
   ngOnInit() {
     this.rol = sessionStorage.getItem('rol');
    this.recargarComunicados(parseInt(this.rol));
    this.cantidadComunicados(parseInt(this.rol));
    console.log("vuelve a cargar");
   


    console.log('rol: '+this.rol);
    this.Usuario = sessionStorage.getItem('nombreUsuario');
    console.log('rol: '+this.rol);
    switch (parseInt( this.rol)) {
      case 1:
          this.RolUsuario = 'Admin';
          break;
      case 2:
          this.RolUsuario = 'Director';
          break;
      case 3:
          this.RolUsuario = 'Administrativo';
          break;
      case 4:
          this.RolUsuario = 'Estudiante';
          break;
      case 5:
          this.RolUsuario = 'Tutor';
          break;
      case 6:
          this.RolUsuario = 'Maestro';
          break;

      default:
        this.RolUsuario = 'Usuario';

        break;
    }
  }
  algo:any;
  async recargarComunicados(rol:number){

    if(rol >2 ){
    this.comunicadoService.GetComunicadosUsuario(parseInt(sessionStorage.getItem('user'))).then(
      
        data=> this.comunicado =data
       
      );

      console.log(this.comunicado);
     }
  }
  async cantidadComunicados(rol:number){
    if(rol>2){
     this.comunicadoService.GetComunicadosCantidad(parseInt(sessionStorage.getItem('user'))).then(
        data=> this.cantidad =data
      );

       console.log(this.cantidad);
     }


  }
  cambioEstadoComunicado(idComunicado:number,idUsuarioReceptor:number,comunicado:any){
    console.log(idComunicado + idUsuarioReceptor);
    console.log(comunicado);
    this.comunicadoService.cambioEstadoComunicado(idComunicado,idUsuarioReceptor);
    this.OpenDialog(comunicado.titulo,comunicado.mensaje,comunicado.tipo);
  }
  OpenDialog(titulo:string,mensaje:string,tipo:string): void{
    const dialogRef = this.dialog.open(VercomunicadoComponent, {
      width: '500px',
      data: { titulo: titulo, mensaje: mensaje, tipo: tipo,
              }
    });

  }
 eventoPublicacion() {
    this.webSocket.listen('agregarMensaje').subscribe((data)=>{
      console.log("emtro al evento");
      this.recargarComunicados(parseInt(this.rol));
      this.cantidadComunicados(parseInt(this.rol));
      //return;
        });
  }
 
}
