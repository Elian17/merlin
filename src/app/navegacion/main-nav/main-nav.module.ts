import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbSidebarModule,NbMenuModule, NbIconModule } from '@nebular/theme';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NbMenuModule,
    NbSidebarModule,
    NbIconModule,
  ]
})
export class MainNavModule { }
