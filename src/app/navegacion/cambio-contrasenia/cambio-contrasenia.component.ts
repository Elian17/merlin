import { Constantes } from './../../Clases/constantes';
import { UsuarioService } from 'src/app/servicios/usuarios.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cambio-contrasenia',
  templateUrl: './cambio-contrasenia.component.html',
  styleUrls: ['./cambio-contrasenia.component.scss']
})
export class CambioContraseniaComponent implements OnInit {

  antigua;
  nueva;
  renueva;
  constructor(private usuarioService: UsuarioService) { }

  ngOnInit() {
  }
  async cambiarContrasenia(){
    if (this.nueva==this.renueva) {
      switch (await this.usuarioService.CambioContrasenia(this.antigua, this.nueva)) {
        case Constantes.Correcto:
          alert("Se actualizó la contraseña correctamente");
          break;
        case Constantes.NoValido:
          alert("No se pudo actualizar la contraseña");
          break;
        case Constantes.ContraseñaAntiguaNoCoincide:
          alert("La contraseña antigua no coincide");
          break;
        default:
          alert("Ha Ocurrido un error");
          break;
      }
    }
    else{
      alert("Los campos de nueva contraseña no coinciden");
    }
  }

}
