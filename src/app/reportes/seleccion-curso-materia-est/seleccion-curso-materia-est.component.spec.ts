import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeleccionCursoMateriaEstComponent } from './seleccion-curso-materia-est.component';

describe('SeleccionCursoMateriaEstComponent', () => {
  let component: SeleccionCursoMateriaEstComponent;
  let fixture: ComponentFixture<SeleccionCursoMateriaEstComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeleccionCursoMateriaEstComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeleccionCursoMateriaEstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
