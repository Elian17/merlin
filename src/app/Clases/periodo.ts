export class Periodo {
    constructor(
        public IdPeriodo: number,
        public HoraInicio: string,
        public HoraFin: string,
        public IdColegio: number,
        public Color: string,
    ) { }
  }
  