export class Horario {
    constructor(
        public IdHorario: number,
        public IdMateria: number,
        public IdCurso: number,
        public IdPeriodo: number,
        public IdColegio: number,
        public Dia: number,
    ) { }
}
