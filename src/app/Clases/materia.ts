export class Materia {
  constructor(
      public IdMateria: number,
      public Nombre: string,
      public Estado: number,
      public IdCurso: number,
      public IdProfesor: number,
      public IdColegio: number
  ) { }
 
}
