export class Curso {
  constructor(
      public IdColegio: number,
      public Grado: number,
      public Nivel: string,
      public Paralelo: string,
      public Estado: number
  ) { }
}
