import { Rol } from './rol';

export class Constantes { static  Correcto = 1;
                          static  NoValido = 2;
                          static  ContraseñaAntiguaNoCoincide = 2;
                          static  SuperAdmin = 1;
                          static  Director = 2;
                          static  Administrativo = 3;
                          static  Estudiante = 4;
                          static  Tutor = 5;
                          static  Maestro = 6;
                          static  Roles = [new Rol(2, "Director"),
                                           new Rol(3, "Administrativo"),
                                           new Rol(4, "Estudiante"),
                                           new Rol(5, "Tutor"),
                                           new Rol(6, "Maestro")
                                          ];
                          }
