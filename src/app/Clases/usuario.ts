export class Usuario {
  constructor(
      public IdUsuario: number,
      public Nombres: string,
      public PrimerApellido?: string,
      public SegundoApellido?: string,
      public Ci?: string,
      public Telefono?: string
  ) { }
}
