import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectorInicioComponent } from './director-inicio.component';

describe('DirectorInicioComponent', () => {
  let component: DirectorInicioComponent;
  let fixture: ComponentFixture<DirectorInicioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectorInicioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectorInicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
