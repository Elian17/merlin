import { Component, OnInit } from '@angular/core';
import { NgxMaterialTimepickerModule, NgxMaterialTimepickerTheme} from 'ngx-material-timepicker';
import { 
  NbToastrService,
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition, 
  NbIconConfig } from '@nebular/theme';
import { Periodo } from '../../Clases/periodo';
import { PeriodosService } from '../../servicios/periodos.service';
import { SessionStorageService } from 'ngx-webstorage';
import { DateTime } from 'luxon';

 
interface objPeriodo{
  horaInicio: string;
  horaFin: string;
  color: string;
  
}

@Component({
  selector: 'app-director-inicio',
  templateUrl: './director-inicio.component.html',
  styleUrls: ['./director-inicio.component.scss']
})
export class DirectorInicioComponent implements OnInit {

  
  constructor(  private toastrService: NbToastrService, private periodoService:PeriodosService,private sessionStorage:SessionStorageService) { }

  ngOnInit() {
    this.recargarPeriodos();
  }

  // Toastr Variables
  //------------------------------------------------------------------
  index = 1;
  destroyByClick = true;
  duration = 4000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbComponentStatus = 'primary';

  title = 'Toast Tittle';
  content = 'Contenido Mensaje';

  //------------------------------------------------------------------

  //---------------Variables para Periodo(s)-----------------------------
    
  periodo : Periodo;
  horaInicio : any;
  horaFin : any;
    
  minHora:any;
  maxHora:any;

  colores =[
    {id:0, color:'#00D68F'},
    {id:1, color:'#A16EFF'},
    {id:2, color:'#0095FF'},
    {id:3, color:'#FFAA00'},
  ];

  periodos:any=[];
 
  //------------------------------------------------------------------

 

/*
  periodos = [
    {horaInicio: '7:30', horaFin:'9:00',nombre:'Periodo', color:'#00D68F'},
    {horaInicio: '9:10', horaFin:'11:00',nombre:'Periodo', color:'#A16EFF'},
    {horaInicio: '11:10', horaFin:'12:30',nombre:'Periodo', color:'#0095FF'},
    {horaInicio: '12:40', horaFin:'13:30',nombre:'Periodo', color:'#FFAA00'},
    {horaInicio: '12:40', horaFin:'13:30',nombre:'Periodo', color:'#FF3D71'},
   
  ];
  */


  darkTheme: NgxMaterialTimepickerTheme = {
    container: {
        bodyBackgroundColor: '#323259',
        buttonColor: '#fff'
    },
    dial: {
        dialBackgroundColor: '#1B1B38',
    },
    clockFace: {
        clockFaceBackgroundColor: '#1B1B38',
        clockHandColor: '#AF87FF',
        clockFaceTimeInactiveColor: '#fff'
    }
  };
  darkTheme2: NgxMaterialTimepickerTheme = {
    container: {
        bodyBackgroundColor: '#323259',
        buttonColor: '#fff'
    },
    dial: {
        dialBackgroundColor: '#1B1B38',
    },
    clockFace: {
        clockFaceBackgroundColor: '#1B1B38',
        clockHandColor: '#AF87FF',
        clockFaceTimeInactiveColor: '#fff'
    }
  };


  async recargarPeriodos(){
    const preLista:any=await this.periodoService.GetPeriodosColegio(parseInt(sessionStorage.getItem('colegio')));
    console.log(preLista);
    preLista.forEach(periodo => {
      const per = new Periodo(periodo.idPeriodo,periodo.horaInicio.substring(0,5),periodo.horaFin.substring(0,5),parseInt(sessionStorage.getItem('colegio')),this.colores[this.periodos.length%4].color);
      this.periodos.push(per);    
      
    });
    console.log(this.periodos);
    console.log(sessionStorage.getItem('colegio'));
    // this.dataSource = new MatTableDataSource(this.Cursos);
  }

  // Método para verificar si las horas se cruzan con un periodo existente (Recibe dos fechas(HORAS))
  periodoExiste(inicio:Date, fin:Date):boolean{
    var cont =0;
    this.periodos.forEach(element => {
      var dInicio = new Date('2000-01-01T'+element.HoraInicio);
      var dFin = new Date('2000-01-01T'+element.HoraFin);
      if ((inicio>=dInicio && inicio<=dFin)||(fin>=dInicio && fin<=dFin)) {
        console.log('existe!');
        cont++;
      }
    });
    if (cont>0) {
      return true;
    } else {
      return false;
    }
  }
  
  // Método agrgar Periodo
  async addPeriodo(){
    if (this.horaInicio != null && this.horaFin != null) {

      // const dateInicio = new Date('2000-01-01T'+this.horaInicio);
      // const dateFin = new Date('2000-01-01T'+this.horaFin);
      var dateInicio = new Date('2000-01-01T'+this.horaInicio);
      var dateFin = new Date('2000-01-01T'+this.horaFin);
      if (this.periodos.length > 0) {
        
        //Verificar
        var existe = this.periodoExiste(dateInicio,dateFin);
  
        console.log(this.periodos.length);
        console.log(this.periodos[0]);
        console.log(existe);
  
        if (existe) {
          //No agregar
          const type = 'danger';
          const tituloT = 'Advertencia';
          const contenido = 'Por favor seleccione horas válidas.';
          const icono = 'close';
  
          this.showToast(type, tituloT, contenido, icono);
        } else {
          //Agregar Periodo 
          const per = new Periodo(this.periodos.length+1,this.horaInicio,this.horaFin,parseInt(sessionStorage.getItem('colegio')),this.colores[this.periodos.length%4].color);
          this.periodos.push(per);    
          this.horaInicio=null;
          this.horaFin=null;
          await this.periodoService.AgregarPeriodoColegio(per);
          this.recargarPeriodos();
          //Ordenar
          this.periodos.sort((val1, val2):any=> {
            return  new Date('2000-01-01T'+val1.HoraInicio).getTime() - 
                    new Date('2000-01-01T'+val2.HoraInicio).getTime();
          });
          const type = 'success';
          const tituloT = 'Periodo agregado';
          const contenido = 'De: ' + dateInicio.toLocaleTimeString().substring(0,5)+ ' A: ' + dateFin.toLocaleTimeString().substring(0,5);
          const icono = 'close';
  
          this.showToast(type, tituloT, contenido, icono);
        }
        
      }
      else{
        //Agregar primer Periodo
        const per = new Periodo(this.periodos.length+1,this.horaInicio,this.horaFin,parseInt(sessionStorage.getItem('colegio')),this.colores[this.periodos.length%4].color);
        this.periodos.push(per);
        this.horaInicio=null;
        this.horaFin=null;
        await this.periodoService.AgregarPeriodoColegio(per);
        this.recargarPeriodos();
        const type = 'success';
        const tituloT = 'Periodo agregado';
        const contenido = 'De: ' + dateInicio.toLocaleTimeString().substring(0,5)+ ' A: ' + dateFin.toLocaleTimeString().substring(0,5);
        const icono = 'close';
  
        this.showToast(type, tituloT, contenido, icono);
      }
    }
    else{
      const type = 'danger';
      const tituloT = 'Advertencia';
      const contenido = 'Por favor seleccione horas válidas.';
      const icono = 'close';

      this.showToast(type, tituloT, contenido, icono);
    }

  }

  async removerPeriodo(id:any){
    //Eliminar Periodo
    console.log('Eliminar periodo:'+id);
    await this.periodoService.EliminarPeriodo(id);
    //Refrescar Lista de periodos
    this.recargarPeriodos();
  }


  // Método para mostrar Toast 
  private showToast(type: NbComponentStatus, title: string, body: string, iconName:string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      icon: { icon: iconName, pack: 'eva' },
      position: this.position,
      preventDuplicates: this.preventDuplicates,
      limit:3,
    };
    const titleContent = title ? `${title}` : '';


    this.index += 1;
    this.toastrService.show( body, `${titleContent}`, config );
  }
}
