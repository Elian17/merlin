import { SessionStorageService } from 'ngx-webstorage';
import { UsuarioService } from './../../servicios/usuarios.service';
import { Component, OnInit } from '@angular/core';
import { Constantes } from 'src/app/Clases/constantes';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  usuario = 'EA218';
  contrasenia = '12345';
  constructor(
    private usuarioService: UsuarioService,
    private rutas:Router,
    private sessionStorage: SessionStorageService) {
    }

  ngOnInit() {
    sessionStorage.clear();
  }

  ingresar() {
    this.usuarioService.Login(this.usuario.trim(), this.contrasenia).then(
      result => {
          switch (result) {
          case Constantes.Correcto:
              switch (parseInt(sessionStorage.getItem('rol'))) {
                case Constantes.SuperAdmin:
                  this.rutas.navigate(['/admin']);
                  break;
                case Constantes.Director:
                  this.rutas.navigate(['/usuariosColegio']);
                  break;
                case Constantes.Administrativo:
                  this.rutas.navigate(['/administrativo']);
                  break;
                case Constantes.Estudiante:
                  this.rutas.navigate(['/estudiante']);
                  break;
                case Constantes.Tutor:
                  this.rutas.navigate(['/tutor']);
                  break;
                case Constantes.Maestro:
                  this.rutas.navigate(['/personal']);
                  break;
                default:
                  alert('Datos ingresados no válidos');
                  console.log('error de rol');
                  break;
              }
              break;
          case Constantes.NoValido:
            alert('Datos ingresados no válidos o Usuario no permitido');
            break;
          default:
            alert('Contacte al admin');
            break;
        }
      },
      error => {
          console.log(error);
      }
    );

    // console.log(this.usuarioService.login(this.usuario, this.contrasenia));
    // .then(res => {console.log(res)});
  }
}

