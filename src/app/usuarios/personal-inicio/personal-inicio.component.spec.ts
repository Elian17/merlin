import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalInicioComponent } from './personal-inicio.component';

describe('PersonalInicioComponent', () => {
  let component: PersonalInicioComponent;
  let fixture: ComponentFixture<PersonalInicioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonalInicioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalInicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
