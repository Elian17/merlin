import { AsistenciaService } from './../../servicios/asistencia.service';
import { Component, OnInit, Injectable } from '@angular/core';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-personal-inicio',
  templateUrl: './personal-inicio.component.html',
  styleUrls: ['./personal-inicio.component.scss']
})

export class PersonalInicioComponent implements OnInit {

  meses: any[] = [];
  cursos: any[] = [];
  curso: any;
  asistencias: any;
  displayedColumns: any[] = [];
  constructor(private asistenciaService: AsistenciaService) { }

  async ngOnInit() {
    this.cursos = await this.asistenciaService.GetCursosProfesor(parseInt(sessionStorage.getItem('user'))) as any[];

  }

  seleccionCurso(curso: any) {
    this.curso = curso;
    this.get();
  }

  async get() {
    this.asistencias = await this.asistenciaService.GetAsistenciasEstudiantesRango(this.curso.idMateria, "0","0","0","0");
    let duplicado = false;
    let mesEnLista;
    this.asistencias.fechas.fechas.forEach(fecha => {
        mesEnLista = parseInt(fecha.mes);
        for (let i = 0; i < this.displayedColumns.length; i++) {
          if (mesEnLista == parseInt(this.displayedColumns[i].nMes)) {
            duplicado = true;
            this.displayedColumns[i].nFechas++;
          }
        }
        if (!duplicado) {
            this.agregarMesLista(mesEnLista);
            duplicado = false;
        }
        duplicado = false;

      });
    this.displayedColumns.sort(function(a,b){return a.nMes - b.nMes;});
  this.asistencias.fechas.fechas.sort(function(a,b){return a.idFecha - b.idFecha;});
  //this.asistencias.fechas.fechas.sort(function(a,b){return a.mes - b.mes;});

  }

  volverCursos() {
    this.curso = undefined;
    this.displayedColumns=[];
    this.meses=[];
    this.asistencias=undefined;
  }

  agregarMesLista(mes) {
    switch (mes) {
      case 1:
        this.displayedColumns.push({nMes: 1, mes: 'Enero', nFechas: 1});
        break;
      case 2:
          this.displayedColumns.push({nMes: 2, mes: 'Febrero', nFechas: 1});
          break;
      case 3:
          this.displayedColumns.push({nMes: 3, mes: 'Marzo', nFechas: 1});
          break;
      case 4:
          this.displayedColumns.push({nMes: 4, mes: 'Abril', nFechas: 1});
          break;
      case 5:
          this.displayedColumns.push({nMes: 5, mes: 'Mayo', nFechas: 1});
          break;
      case 6:
          this.displayedColumns.push({nMes: 6, mes: 'Junio', nFechas: 1});
          break;
      case 7:
          this.displayedColumns.push({nMes: 7, mes: 'Julio', nFechas: 1});
          break;
      case 8:
          this.displayedColumns.push({nMes: 8, mes: 'Agosto', nFechas: 1});
          break;
      case 9:
          this.displayedColumns.push({nMes: 9, mes: 'Septiembre', nFechas: 1});
          break;
      case 10:
          this.displayedColumns.push({nMes: 10, mes: 'Octubre', nFechas: 1});
          break;
      case 11:
          this.displayedColumns.push({nMes: 11, mes: 'Noviembre', nFechas: 1});
          break;
      case 12:
          this.displayedColumns.push({nMes: 12, mes: 'Diciembre', nFechas: 1});
          break;

      default:
        break;
    }
  }


}

