import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecretariaInicioComponent } from './secretaria-inicio.component';

describe('SecretariaInicioComponent', () => {
  let component: SecretariaInicioComponent;
  let fixture: ComponentFixture<SecretariaInicioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecretariaInicioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecretariaInicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
