import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TutorInicioComponent } from './tutor-inicio.component';

describe('TutorInicioComponent', () => {
  let component: TutorInicioComponent;
  let fixture: ComponentFixture<TutorInicioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TutorInicioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TutorInicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
