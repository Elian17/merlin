import { SessionStorageService } from 'ngx-webstorage';
import { ColegioService } from './../../servicios/colegios.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NuevoColegioComponent } from './../../admin/nuevo-colegio/nuevo-colegio.component';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { Usuario } from 'src/app/Clases/usuario';
import { Colegio } from 'src/app/Clases/colegio';
import { Router } from '@angular/router';
import { EditarColegioComponent } from 'src/app/admin/editar-colegio/editar-colegio.component';

export interface DialogData {
  rol: number;

  nuevo: boolean;
  nombreColegio: string;
  nombres: string;
  primerApellido: string;
  segundoApellido: string;
  ci: string;
  telefono: string;

  tutores: any[];

}

@Component({
  selector: 'app-admin-inicio',
  templateUrl: './admin-inicio.component.html',
  styleUrls: ['./admin-inicio.component.scss']
})
export class AdminInicioComponent implements OnInit {




  Coles: any;

  nombreColegio: string;
  nombres: string;
  primerApellido: string;
  segundoApellido: string;
  ci: string;
  telefono: string;

  animal: string;
  name: string;

  displayedColumns: string[] = ['nombreColegio', 'Ver', 'Estado','Editar'];
  dataSource = this.Coles;

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort,{static: false}) sort: MatSort;

  constructor(public dialog: MatDialog,
              private sessionStorage: SessionStorageService,
              private colegioService: ColegioService, private rutas:Router) { }

  ngOnInit() {
    this.recargarColegios();
  }


  openDialog(): void {
    const dialogRef = this.dialog.open(NuevoColegioComponent, {
      width: '500px',
      data: { nombreColegio: this.nombreColegio, nombres: this.nombres, primerApellido: this.primerApellido,
              segundoApellido: this.segundoApellido, ci: this.ci, telefono: this.telefono}
    });

    dialogRef.afterClosed().subscribe(async result => {
    await this.agregarColegio(new Usuario(
                                            0, result.nombres,
                                            result.primerApellido,
                                            result.segundoApellido,
                                            result.ci,
                                            result.telefono
      ), result.nombreColegio);
    this.recargarColegios();

    });
  }
  async agregarColegio(usuario: Usuario, colegio: string){
    await this.colegioService.AgregarColegioDirector(usuario, new Colegio(0, colegio));
  }

  VerDetalles(idColegio: number){

    console.log(sessionStorage.getItem('user'));
    console.log(sessionStorage.getItem('rol'));
    console.log(idColegio);
    this.rutas.navigate(['/detallecolegio',idColegio]);
  }

  async CambioEstado(idColegio: number,estado:number){
    console.log(idColegio);
    if (estado ==1){
        this.colegioService.DeshabilitarColegio(idColegio);
    } else {
      this.colegioService.HabilitarColegio(idColegio);
    }
    this.recargarColegios();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  async recargarColegios() {
    this.Coles =await this.colegioService.GetColegios();

    this.dataSource = new MatTableDataSource(this.Coles);
  }

  OpenDialogEditar(idColegio: number, nombreColegio: string): void {
    const dialogRef = this.dialog.open(EditarColegioComponent, {
      width: '500px',
      data: { idColegio, nombreColegio}
    });

    dialogRef.afterClosed().subscribe(async result => {
    await this.modificarColegio(new Colegio(
                                             result.idColegio,
                                            result.nombreColegio,

      ));
    this.recargarColegios();

    });
  }
  async modificarColegio(cole: Colegio) {
    await this.colegioService.ModificarColegio(cole);
  }
}
