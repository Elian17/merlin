import { Component, OnInit, Inject} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogData } from 'src/app/usuarios/admin-inicio/admin-inicio.component';

@Component({
  selector: 'app-nuevo-colegio',
  templateUrl: './nuevo-colegio.component.html',
  styleUrls: ['./nuevo-colegio.component.scss']
})
export class NuevoColegioComponent implements OnInit {

  nombreColegio: string;
  nombres: string;
  primerApellido: string;
  segundoApellido: string;
  ci: string;
  telefono: string;

  constructor(  public dialogRef:MatDialogRef<NuevoColegioComponent>,
                @Inject(MAT_DIALOG_DATA) public data:DialogData) { }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
