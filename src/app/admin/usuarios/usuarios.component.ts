import { Constantes } from './../../Clases/constantes';
import { NuevoAdminComponent } from './../nuevo-admin/nuevo-admin.component';

import { UsuarioService } from './../../servicios/usuarios.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NuevoColegioComponent } from './../../admin/nuevo-colegio/nuevo-colegio.component';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { Usuario } from 'src/app/Clases/usuario';
import { SessionStorageService } from 'ngx-webstorage';



@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent implements OnInit {


  idSesion;


  Usuarios: any;

  nombreColegio: string;
  nombres: string;
  primerApellido: string;
  segundoApellido: string;
  ci: string;
  telefono: string;

  displayedColumns: string[] = ['nombreUsuario', 'nombre', 'telefono', 'Estado', 'Editar'];
  dataSource = this.Usuarios;

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort,{static: false}) sort: MatSort;

  constructor(public dialog: MatDialog,
    private usuarioService: UsuarioService,
    private sessionStorage: SessionStorageService) { }

  ngOnInit() {
    this.getUsuarios();
    this.idSesion=1;
  }

  async getUsuarios()
  {
    this.Usuarios = await this.usuarioService.GetSuperAdmin();
    this.dataSource = new MatTableDataSource(this.Usuarios);
  }


  openDialog(): void {
    const dialogRef = this.dialog.open(NuevoAdminComponent, {
      width: '500px',
      data: { nuevo: true, nombres: this.nombres, primerApellido: this.primerApellido,
              segundoApellido: this.segundoApellido, ci: this.ci, telefono: this.telefono}
    });

    var user;
    dialogRef.afterClosed().subscribe(async result => {
      user = await this.usuarioService.AgregarSuperAdmin(new Usuario(0, result.nombres, result.primerApellido, result.segundoApellido, result.ci, result.telefono));
      this.getUsuarios();
      console.log(user);
    });
  }

  editar(user: any) {
    const dialogRef = this.dialog.open(NuevoAdminComponent, {
      width: '500px',
      data: { nuevo: false, idUsuarioMaster: user.idUsuarioMaster, nombres: user.nombres, primerApellido: user.primerApellido,
              segundoApellido: user.segundoApellido, ci: user.ci, telefono: user.telefono}
    });

    dialogRef.afterClosed().subscribe(async result => {
      await this.usuarioService.ModificarUsuarioMaster(new Usuario(result.idUsuarioMaster, result.nombres, result.primerApellido, result.segundoApellido, result.ci, result.telefono));
      this.getUsuarios();
    });
  }
  async Habilitar(idUsuario: number){
    await this.usuarioService.HabilitarSuperAdmin(idUsuario);
    this.getUsuarios();
  }

  async Inhabilitar(idUsuario: number){
    switch (await this.usuarioService.InhabilitarSuperAdmin(idUsuario)) {
      case Constantes.Correcto:
        this.getUsuarios();
        alert("El usuario ha sido inhabilitado");
        break;
      case 2:
        alert("No tiene los permisos suficientes para realizar esta acción");
        break;
      default:
        break;
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
