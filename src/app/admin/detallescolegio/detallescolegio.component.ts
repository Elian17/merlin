import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ColegioService } from 'src/app/servicios/colegios.service';

@Component({
  selector: 'app-detallescolegio',
  templateUrl: './detallescolegio.component.html',
  styleUrls: ['./detallescolegio.component.scss']
})
export class DetallescolegioComponent implements OnInit {
  hola:string;
   Coles={
    idColegios:null,
    nombreColegio:null,
    estadoColegio:null
  };
  cole:any;
  Usuario:any;
  displayedColumns: string[] = ['Nombre Completo', 'Ci', 'Telefono','Nombre Usuario'];
  dataSource = this.Usuario;

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort,{static: false}) sort: MatSort;
  constructor(private router:ActivatedRoute,private colSer:ColegioService,private rout:Router) { 
    this.hola="Juan";
  }

  async ngOnInit() {
    let idcolegio = parseInt(this.router.snapshot.paramMap.get('idColegio'));
   //this.Coles =  await this.colSer.GetIdColegio(idcolegio);
   this.colSer.GetIdColegio(idcolegio).then(
    result => {
        this.cole=result;
        this.cole.forEach(element => {
          this.Coles.idColegios =element.idColegio;
          this.Coles.nombreColegio= element.nombreColegio;
           if(element.estado==1){
                this.Coles.estadoColegio = "Habilitado";
           }
           else{
            this.Coles.estadoColegio = "InHabilitado";
           }
        });
         
       },
        error => {
            console.log(error);
        }
      );

   this.Usuario = await this.colSer.GetDirectorColegio(idcolegio);
   this.dataSource = new MatTableDataSource(this.Usuario);

  // console.log(this.Coles[0][1].nombreColegio);

  }
  volver(){
    this.rout.navigate(['admin']);
  }
}
