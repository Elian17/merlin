import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallescolegioComponent } from './detallescolegio.component';

describe('DetallescolegioComponent', () => {
  let component: DetallescolegioComponent;
  let fixture: ComponentFixture<DetallescolegioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetallescolegioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallescolegioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
