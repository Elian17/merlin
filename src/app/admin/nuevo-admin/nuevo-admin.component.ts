import { Component, OnInit, Inject} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogData } from 'src/app/usuarios/admin-inicio/admin-inicio.component';
import { Usuario } from 'src/app/Clases/usuario';

@Component({
  selector: 'app-nuevo-admin',
  templateUrl: './nuevo-admin.component.html',
  styleUrls: ['./nuevo-admin.component.scss']
})
export class NuevoAdminComponent implements OnInit {

  nuevo: boolean;
  nombres: string;
  primerApellido: string;
  segundoApellido: string;
  ci: string;
  telefono: string;

  constructor(public dialogRef:MatDialogRef<NuevoAdminComponent>,
                @Inject(MAT_DIALOG_DATA) public data:DialogData) { }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
