import { Component, OnInit, Inject } from '@angular/core';
import { DialogData } from 'src/app/usuarios/admin-inicio/admin-inicio.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-editar-colegio',
  templateUrl: './editar-colegio.component.html',
  styleUrls: ['./editar-colegio.component.scss']
})
export class EditarColegioComponent implements OnInit {
  idColegio:number;
  nombreColegio: string;
  constructor( public dialogRef:MatDialogRef<EditarColegioComponent>,
    @Inject(MAT_DIALOG_DATA) public data:DialogData,) { }

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
