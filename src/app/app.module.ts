import { NotasService } from './servicios/notas.service';
import { TutorService } from './servicios/tutor.service';
import { MateriaService } from './servicios/materia.service';
import { HorarioService } from './servicios/horario.service';
import { EstudianteService } from './servicios/estudiante.service';
import { CursosService } from './servicios/cursos.service';
import { AsistenciaService } from './servicios/asistencia.service';
import { ColegioService } from './servicios/colegios.service';
import { UsuarioService } from './servicios/usuarios.service';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';

import { NgxWebstorageModule } from 'ngx-webstorage';
import { RouterModule } from '@angular/router';

import { NbEvaIconsModule } from '@nebular/eva-icons';
import {
          NbThemeModule,
          NbContextMenuModule,
          NbActionsModule,
          NbMenuModule,
          NbSidebarModule,
          NbLayoutModule,
          NbButtonModule,
          NbInputModule,
          NbDialogModule,
          NbCardModule,
          NbUserModule,
          NbCheckboxModule,
          NbRadioModule,
          NbDatepickerModule,
          NbSelectModule,
          NbIconModule,
          NbTooltipModule,
          NbListModule,
          NbAccordionModule,
          NbToastrModule
        } from '@nebular/theme';
import { Ang_Material_imports } from './imports/AngularMat_Imports';
import {
  // MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  // MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';

import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';

import {MatAutocompleteModule,MatInputModule} from '@angular/material';
import { AppComponent } from './app.component';
import { LoginComponent } from './usuarios/login/login.component';
import { AdminInicioComponent } from './usuarios/admin-inicio/admin-inicio.component';
import { NuevoColegioComponent } from './admin/nuevo-colegio/nuevo-colegio.component';
import { EditarColegioComponent } from './admin/editar-colegio/editar-colegio.component';
import { SecretariaInicioComponent } from './usuarios/secretaria-inicio/secretaria-inicio.component';
import { TutorInicioComponent } from './usuarios/tutor-inicio/tutor-inicio.component';
import { EstudianteInicioComponent } from './usuarios/estudiante-inicio/estudiante-inicio.component';
import { PersonalInicioComponent } from './usuarios/personal-inicio/personal-inicio.component';
import { MainNavComponent } from './navegacion/main-nav/main-nav.component';
import { LayoutComponent } from './navegacion/layout/layout.component';
import { from } from 'rxjs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UsuariosComponent } from './admin/usuarios/usuarios.component';
import { NuevoAdminComponent } from './admin/nuevo-admin/nuevo-admin.component';
import { NuevoUsuarioComponent } from './director/nuevo-usuario/nuevo-usuario.component';
import { CursosComponent } from './director/cursos/cursos.component';
import { NuevoCursoComponent } from './director/nuevo-curso/nuevo-curso.component';
import { MateriasComponent } from './director/materias/materias.component';
import { NuevaMateriaComponent } from './director/nueva-materia/nueva-materia.component';
import { GestionesComponent } from './director/gestiones/gestiones.component';
import { NuevaGestionComponent } from './director/nueva-gestion/nueva-gestion.component';
import { DirectorInicioComponent } from './usuarios/director-inicio/director-inicio.component';
import { UsuariosColegioComponent } from './director/usuarios/usuariosColegio.component';
import { DetallescolegioComponent } from './admin/detallescolegio/detallescolegio.component';
import { CambioContraseniaComponent } from './navegacion/cambio-contrasenia/cambio-contrasenia.component';
import { AsistenciaComponent } from './personal/asistencia/asistencia.component';
import { ComunicadoComponent } from './comunicados/comunicado/comunicado.component';
import { CrearComunicadoComponent } from './comunicados/crear-comunicado/crear-comunicado.component';
import { UpdateComunicadoComponent } from './comunicados/update-comunicado/update-comunicado.component';
import { VercomunicadoComponent } from './comunicados/vercomunicado/vercomunicado.component';
import { HorariosComponent } from './personal/horarios/horarios.component';
import { NotasComponent } from './personal/notas/notas.component';
import { VistaNotasComponent } from './personal/vista-notas/vista-notas.component';
import { NuevaNotaComponent } from './personal/nueva-nota/nueva-nota.component';
import { SeleccionCursoMateriaEstComponent } from './reportes/seleccion-curso-materia-est/seleccion-curso-materia-est.component';
import { ReportesComponent } from './reportes/reportes/reportes.component';
import { PeriodosComponent } from './personal/periodos/periodos.component';
import { CursoHorarioComponent } from './personal/curso-horario/curso-horario.component';
import { NuevoHorarioComponent } from './personal/nuevo-horario/nuevo-horario.component';
import { InscripcionesEstudianteComponent } from './inscripciones/inscripciones-estudiante/inscripciones-estudiante.component';
import { NuevainscripcionComponent } from './inscripciones/nuevainscripcion/nuevainscripcion.component';
import { VerasistenciaComponent } from './estudiante/verasistencia/verasistencia.component';
import { VernotasComponent } from './estudiante/vernotas/vernotas.component';
import { VerNotasTutorComponent } from './tutor/ver-notas-tutor/ver-notas-tutor.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AdminInicioComponent,
    NuevoColegioComponent,
    EditarColegioComponent,
    SecretariaInicioComponent,
    TutorInicioComponent,
    EstudianteInicioComponent,
    PersonalInicioComponent,
    MainNavComponent,
    LayoutComponent,
    UsuariosComponent,
    UsuariosColegioComponent,
    NuevoAdminComponent,
    NuevoUsuarioComponent,
    CursosComponent,
    NuevoCursoComponent,
    MateriasComponent,
    NuevaMateriaComponent,
    GestionesComponent,
    NuevaGestionComponent,
    DirectorInicioComponent,
    DetallescolegioComponent,
    CambioContraseniaComponent,
    AsistenciaComponent,
    ComunicadoComponent,
    CrearComunicadoComponent,
    UpdateComunicadoComponent,
    VercomunicadoComponent,
    HorariosComponent,
    NotasComponent,
    VistaNotasComponent,
    NuevaNotaComponent,
    SeleccionCursoMateriaEstComponent,
    ReportesComponent,
    PeriodosComponent,
    CursoHorarioComponent,
    NuevoHorarioComponent,
    InscripcionesEstudianteComponent,
    NuevainscripcionComponent,
    VerasistenciaComponent,
    VernotasComponent,
    VerNotasTutorComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgxWebstorageModule.forRoot(),
    RouterModule, // RouterModule.forRoot(routes, { useHash: true }), if this is your app.module
    NbLayoutModule,
    NbSidebarModule.forRoot(), // NbSidebarModule.forRoot(), //if this is your app.module
    NbButtonModule,
    NbThemeModule.forRoot({ name: 'cosmic' }),
    NbActionsModule,
    NbMenuModule.forRoot(),
    NbContextMenuModule,
    NbInputModule,
    NbDialogModule.forRoot(),
    NbCardModule,
    NbUserModule,
    NbCheckboxModule,
    NbRadioModule,
    NbSelectModule,
    NbIconModule,
    NbEvaIconsModule,
    NbTooltipModule,
    NbAccordionModule,
    BrowserAnimationsModule,
    Ang_Material_imports,
    NgxMaterialTimepickerModule,
    NbToastrModule.forRoot(),
    MatAutocompleteModule,
   NbDatepickerModule.forRoot(),
   NbDatepickerModule,

    NbDatepickerModule.forRoot(),
    NbDatepickerModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
  NgSelectModule,
  NbListModule

  ],
  providers: [UsuarioService, ColegioService, AsistenciaService, CursosService,
              EstudianteService, HorarioService, MateriaService, TutorService,
              NotasService],
  bootstrap: [AppComponent],
  entryComponents: [NuevoColegioComponent, NuevoAdminComponent, NuevoUsuarioComponent, NuevaNotaComponent, NuevoHorarioComponent]
})
export class AppModule { }
