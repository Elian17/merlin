import { SeleccionCursoMateriaEstComponent } from './reportes/seleccion-curso-materia-est/seleccion-curso-materia-est.component';
import { ReportesComponent } from './reportes/reportes/reportes.component';
import { NotasComponent } from './personal/notas/notas.component';
import { CambioContraseniaComponent } from './navegacion/cambio-contrasenia/cambio-contrasenia.component';
import { UsuariosColegioComponent } from './director/usuarios/usuariosColegio.component';
import { DirectorInicioComponent } from './usuarios/director-inicio/director-inicio.component';
import { UsuariosComponent } from './admin/usuarios/usuarios.component';
import { EstudianteInicioComponent } from './usuarios/estudiante-inicio/estudiante-inicio.component';
import { TutorInicioComponent } from './usuarios/tutor-inicio/tutor-inicio.component';
import { PersonalInicioComponent } from './usuarios/personal-inicio/personal-inicio.component';
import { SecretariaInicioComponent } from './usuarios/secretaria-inicio/secretaria-inicio.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { LoginComponent } from './usuarios/login/login.component';
import { LayoutComponent } from './navegacion/layout/layout.component';
import { AdminInicioComponent } from './usuarios/admin-inicio/admin-inicio.component';
import { NuevoColegioComponent } from './admin/nuevo-colegio/nuevo-colegio.component';
import { DetallescolegioComponent } from './admin/detallescolegio/detallescolegio.component';
import { EditarColegioComponent } from './admin/editar-colegio/editar-colegio.component';
import { CursosComponent } from './director/cursos/cursos.component';
import { NuevoCursoComponent } from './director/nuevo-curso/nuevo-curso.component';
import { MateriasComponent } from './director/materias/materias.component';
import { NuevaMateriaComponent } from './director/nueva-materia/nueva-materia.component';
import { ComunicadoComponent } from './comunicados/comunicado/comunicado.component';
import { CrearComunicadoComponent } from './comunicados/crear-comunicado/crear-comunicado.component';
import { UpdateComunicadoComponent } from './comunicados/update-comunicado/update-comunicado.component';
import { VercomunicadoComponent } from './comunicados/vercomunicado/vercomunicado.component';
import { HorariosComponent } from './personal/horarios/horarios.component';
import { PeriodosComponent } from './personal/periodos/periodos.component';
import { CursoHorarioComponent } from './personal/curso-horario/curso-horario.component';
import { InscripcionesService } from './servicios/inscripciones.service';
import { InscripcionesEstudianteComponent } from './inscripciones/inscripciones-estudiante/inscripciones-estudiante.component';
import { NuevainscripcionComponent } from './inscripciones/nuevainscripcion/nuevainscripcion.component';
import { AsistenciaComponent } from './personal/asistencia/asistencia.component';
import { VerasistenciaComponent } from './estudiante/verasistencia/verasistencia.component';
import { VernotasComponent } from './estudiante/vernotas/vernotas.component';
import { VerNotasTutorComponent } from './tutor/ver-notas-tutor/ver-notas-tutor.component';


const routes: Routes = [
  {path: '', component: LoginComponent},
  {
    path: 'cambioContrasenia', component: LayoutComponent,
    children: [
      {path: '', component: CambioContraseniaComponent},

    ]
  },
  {
    path: 'admin', component: LayoutComponent,
    children: [
      {path: '', component: AdminInicioComponent},

    ]
  },
  {
    path: 'usuarios', component: LayoutComponent,
    children: [
      {path: '', component: UsuariosComponent},

    ]
  },
  {
    path: 'usuariosColegio', component: LayoutComponent,
    children: [
      {path: '', component: UsuariosColegioComponent},

    ]
  },
  {
    path: 'director', component: LayoutComponent,
    children: [
      {path: '', component: DirectorInicioComponent},

    ]
  },
  {
    path: 'administrativo', component: LayoutComponent,
    children: [
      {path: '', component: SecretariaInicioComponent},

    ]
  },
  {
    path: 'personal', component: LayoutComponent,
    children: [
      {path: '', component: PersonalInicioComponent},

    ]
  },
  {
    path: 'tutor', component: LayoutComponent,
    children: [
      {path: '', component: TutorInicioComponent},

    ]
  },
  {
    path: 'estudiante', component: LayoutComponent,
    children: [
      {path: '', component: EstudianteInicioComponent},

    ]
  },
  {
    path: 'nuevocole', component: LayoutComponent,
    children: [
      {path: '', component: NuevoColegioComponent},

    ]
  },
  {
  path: 'detallecolegio/:idColegio', component: LayoutComponent,
  children: [
    {path: '', component: DetallescolegioComponent},

  ]
},
{
  path: 'editarColegio', component: LayoutComponent,
  children: [
    {path: '', component: EditarColegioComponent},

  ]
},
{
  path: 'cursos', component: LayoutComponent,
  children: [
    {path: '', component: CursosComponent},

  ]
},
{
  path: 'nuevoCurso', component: LayoutComponent,
  children: [
    {path: '', component: NuevoCursoComponent},

  ]
},
{
  path: 'materia', component: LayoutComponent,
  children: [
    {path: '', component:MateriasComponent},

  ]
},
{
  path: 'nuevaMateria', component: LayoutComponent,
  children: [
    {path: '', component: NuevaMateriaComponent},

  ]
},
{
  path: 'asistencias', component: LayoutComponent,
  children: [
    {path: '', component: PersonalInicioComponent},

  ]
},
{
  path: 'comunicado', component: LayoutComponent,
  children: [
    {path: '', component: ComunicadoComponent},

  ]
},
{
  path: 'nuevoComunicado', component: LayoutComponent,
  children: [
    {path: '', component:CrearComunicadoComponent},

  ]
},
{
  path: 'updateComunicado', component: LayoutComponent,
  children: [
    {path: '', component:UpdateComunicadoComponent},

  ]
},
{
  path: 'vercomunicado', component: LayoutComponent,
  children: [
    {path: '', component:VercomunicadoComponent},

  ]
},
{
  path: 'horarios', component: LayoutComponent,
  children: [
    {path: '', component: HorariosComponent},

  ]
},
{
  path: 'notas', component: LayoutComponent,
  children: [
    {path: '', component: NotasComponent},

  ]
},
{
  path: 'reportes', component: LayoutComponent,
  children: [
    {path: '', component: ReportesComponent},

  ]
},
{
  path: 'seleccion-curso-materia', component: LayoutComponent,
  children: [
    {path: '', component: SeleccionCursoMateriaEstComponent},

  ]
},
{
  path: 'inscripcionesEstudiante', component: LayoutComponent,
  children: [
    {path: '', component: InscripcionesEstudianteComponent},

  ]
},
{
  path: 'nuevaInscripcion', component: LayoutComponent,
  children: [
    {path: '', component: NuevainscripcionComponent},

  ]
},
{
  path: 'verAsistenciaEstudiante', component: LayoutComponent,
  children: [
    {path: '', component: VerasistenciaComponent},

  ]
},
{
  path: 'vernotasEstudiante', component: LayoutComponent,
  children: [
    {path: '', component: VernotasComponent},

  ]
},
{
  path: 'vernotasTutor', component: LayoutComponent,
  children: [
    {path: '', component: VerNotasTutorComponent},

  ]
},
{
  path: 'tutorInicio', component: LayoutComponent,
  children: [
    {path: '', component: TutorInicioComponent},

  ]
},
{
  path: 'cursoHorario', component: LayoutComponent,
  children: [
    {path: '', component: CursoHorarioComponent},

  ]
}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
