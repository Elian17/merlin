import { SessionStorageService } from 'ngx-webstorage';
import { Sesion } from './../Clases/sesion';
import { Constantes } from './../Clases/constantes';
import { Usuario } from './../Clases/usuario';
import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule, HttpResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Md5 } from 'ts-md5/dist/md5';


@Injectable()

export class UsuarioService {
  private url: string;
  public results: [];
  httpOptions = {
      headers: new HttpHeaders({
          'Content-Type': 'application/json'
      })
  };

  constructor(
    private _http: HttpClient, private sessionStorage: SessionStorageService
  ) {
      this.url = 'http://trial-bin.000webhostapp.com/colegio/index.php';
  }

  private encriptar(pass: string) {
    return Md5.hashStr(pass);
  }


  Login(usuario: string, contrasenia: string) {
    const urlAux = this.url + '/usuario/iniciarSesion';
    const body = {
      'nombreUsuario': usuario,
      'password': this.encriptar(contrasenia)
    };

    return  new Promise((resolve, reject) => {
     this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
        .then(
          res => {
            console.log(res);
            switch (res.mensaje) {
              case Constantes.Correcto:
                  if (res.rol==Constantes.SuperAdmin) {
                    sessionStorage.setItem('user', res.idUsuarioMaster);
                  }
                  else
                  {
                    sessionStorage.setItem('user', res.idUsuario);
                    sessionStorage.setItem('colegio', res.idColegio);
                  }
                  sessionStorage.setItem('nombreUsuario', res.nombreUsuario);
                  sessionStorage.setItem('rol', res.rol);
                  resolve(Constantes.Correcto);
                  break;

              case Constantes.NoValido:
                resolve(Constantes.NoValido);
                break;

              default:
                resolve(Constantes.NoValido);
                break;
            }

          },
          msg => {
            reject(msg);
          }
        );
    });
  }

  AgregarSuperAdmin(usuario: Usuario) {
    const urlAux = this.url + '/usuariomaster/agregarBDUsuarioMaster';
    const body = {
      'nombres': usuario.Nombres,
      'primerApellido': usuario.PrimerApellido,
      'segundoApellido': usuario.SegundoApellido,
      'ci': usuario.Ci,
      'telefono': usuario.Telefono,
      'idUsuarioVerificar': sessionStorage.getItem('user')
    };
    return this.RetornarPromesa(urlAux, body);
  }

  AgregarDirector(usuario: Usuario, idColegio: number) {
    const urlAux = this.url + '/director/agregarBDDirector';
    const body = {
      'nombres': usuario.Nombres,
      'primerApellido': usuario.PrimerApellido,
      'segundoApellido': usuario.SegundoApellido,
      'ci': usuario.Ci,
      'telefono': usuario.Telefono,
      'idColegio': idColegio,
      'idUsuarioVerificar': sessionStorage.getItem('user')
    };
    return this.RetornarPromesa(urlAux, body);
  }
  AgregarSecretaria(usuario: Usuario, idColegio: number) {
    const urlAux = this.url + '/secretaria/agregarBDSecretaria';
    const body = {
      'nombres': usuario.Nombres,
      'primerApellido': usuario.PrimerApellido,
      'segundoApellido': usuario.SegundoApellido,
      'ci': usuario.Ci,
      'telefono': usuario.Telefono,
      'idColegio': idColegio,
      'idUsuarioVerificar': sessionStorage.getItem('user')
    };
    return this.RetornarPromesa(urlAux, body);
  }

  AgregarTutor(usuario: Usuario, idColegio: number) {
    const urlAux = this.url + '/tutor/agregarBDTutor';
    const body = {
      'nombres': usuario.Nombres,
      'primerApellido': usuario.PrimerApellido,
      'segundoApellido': usuario.SegundoApellido,
      'ci': usuario.Ci,
      'telefono': usuario.Telefono,
      'idColegio': idColegio,
      'idUsuarioVerificar': sessionStorage.getItem('user')
    };
    return this.RetornarPromesa(urlAux, body);
  }

  AgregarEstudiante(usuario: Usuario, idColegio: number, tutores: number[]) {
    const urlAux = this.url + '/estudiante/agregarBDEstudiante';
    const body = {
      'nombres': usuario.Nombres,
      'primerApellido': usuario.PrimerApellido,
      'segundoApellido': usuario.SegundoApellido,
      'ci': usuario.Ci,
      'telefono': usuario.Telefono,
      'idColegio': idColegio,
      'idUsuarioVerificar': sessionStorage.getItem('user'),
      'tutor': tutores
    };
    console.log(tutores);
    return this.RetornarPromesa(urlAux, body);
  }

  AgregarMaestro(usuario: Usuario, idColegio: number) {
    const urlAux = this.url + '/maestro/agregarBDMaestro';
    const body = {
      'nombres': usuario.Nombres,
      'primerApellido': usuario.PrimerApellido,
      'segundoApellido': usuario.SegundoApellido,
      'ci': usuario.Ci,
      'telefono': usuario.Telefono,
      'idColegio': idColegio,
      'idUsuarioVerificar': sessionStorage.getItem('user')
    };
    return this.RetornarPromesa(urlAux, body);
  }

  ModificarUsuario(usuario: Usuario){
    console.log(usuario);
    const urlAux = this.url + '/usuario/modificarUsuarioBd';
    const body = {
      'nombres': usuario.Nombres,
      'primerApellido': usuario.PrimerApellido,
      'segundoApellido': usuario.SegundoApellido,
      'ci': usuario.Ci,
      'telefono': usuario.Telefono,
      'idUsuario': usuario.IdUsuario
    };
    return this.RetornarPromesa(urlAux, body);
  }

  ModificarUsuarioMaster(usuario: Usuario){
    console.log(usuario);
    const urlAux = this.url + '/usuariomaster/modificarUsuarioMaster';
    const body = {
      'nombres': usuario.Nombres,
      'primerApellido': usuario.PrimerApellido,
      'segundoApellido': usuario.SegundoApellido,
      'ci': usuario.Ci,
      'telefono': usuario.Telefono,
      'idUsuario': usuario.IdUsuario
    };
    return this.RetornarPromesa(urlAux, body);
  }

  HabilitarUsuario(idUsuario: number){
    const urlAux = this.url + '/usuario/usuarioHabilitar';
    const body = {
      'idUsuario': idUsuario
    };
    return this.RetornarPromesa(urlAux, body);
  }

  InhabilitarUsuario(idUsuario: number){
    const urlAux = this.url + '/usuario/usuarioDeshabilitar';
    const body = {
      'idUsuario': idUsuario
    };
    console.log(body);
    return this.RetornarPromesa(urlAux, body);
  }
  HabilitarSuperAdmin(idUsuario: number){
    const urlAux = this.url + '/usuariomaster/usuarioMasterHabilitar';
    const body = {
      'idUsuario': idUsuario
    };
    return this.RetornarPromesa(urlAux, body);
  }

  InhabilitarSuperAdmin(idUsuario: number){
    const urlAux = this.url + '/usuariomaster/usuarioMasterDeshabilitar';
    const body = {
      'idUsuario': idUsuario
    };
    console.log(body);
    return this.RetornarPromesa(urlAux, body);
  }

  //Devuelve el nombre completo y el id
  BuscarUsuariosColegioNombreCompleto(busqueda: string, tipoUsuario: number, idColegio: number){
    const urlAux = this.url + '/usuario/buscarUsuario';
    const body = {
      "busqueda": busqueda,
      "tipoUsuario": tipoUsuario,
      "idColegio": idColegio
    };

    return  new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
            console.log(res);
             resolve(res);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }
  //Devuelve todos los campos por separados
  BuscarUsuariosColegio(busqueda: string, tipoUsuario: number, idColegio: number){
    const urlAux = this.url + '/usuario/buscarUsuario';
    const body = {
      "busqueda": busqueda,
      "tipoUsuario": tipoUsuario,
      "idColegio": idColegio
    };
    console.log(body);
    return  new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             console.log(res);
             resolve(res);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }

  CambioContrasenia(antigua: string, nueva: string){
    const urlAux = this.url + '/usuario/cambiarContrasena';
    const body = {
      'antiguaContrasena': this.encriptar(antigua),
      'nuevaContrasena': this.encriptar(nueva),
      'idUsuario': sessionStorage.getItem('user')
    };
    return  new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             console.log(res);
             resolve(res.mensaje);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }

  GetSuperAdmin(){
    return new Promise((resolve, reject) => {
      let urlAux = this.url+"/usuariomaster/getSuperAdmin";
      this._http.get(urlAux)
        .map(res => JSON.parse(JSON.stringify(res)))
        .subscribe(
          result => {console.log(result); resolve(result as Usuario[]);
          },
          err => { reject(err); });
    });
  }

  RetornarPromesa(urlAux: string, body: any){
    return  new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             console.log(res);
             resolve(res.mensaje);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }

}
