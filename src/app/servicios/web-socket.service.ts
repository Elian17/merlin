import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { observable, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {
  readonly url:string="http://localhost:3000";
  constructor() {
    this.socket=io(this.url);
   }
   socket :any;
   listen(eventName:string){
    return new Observable((subscriber)=>{
      this.socket.on(eventName, (data)=>{
        subscriber.next(data);
      })
    })
  }
  emit(eventName:string,data:any){
    this.socket.emit(eventName,data);
  }
}
