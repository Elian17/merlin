import { Sesion } from 'src/app/Clases/sesion';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Usuario } from '../Clases/usuario';
import { Colegio } from '../Clases/colegio';


@Injectable()

export class ColegioService {
  private url: string;
  httpOptions = {
      headers: new HttpHeaders({
          'Content-Type': 'application/json'
      })
  }

  constructor(
    private _http: HttpClient,
  ) {
      this.url = "http://trial-bin.000webhostapp.com/colegio/index.php";
  }

  GetColegios() {
    return new Promise((resolve, reject) => {
      let urlAux = this.url+"/colegio/getColegios";
      this._http.get(urlAux)
        .map(res => JSON.parse(JSON.stringify(res)))
        .subscribe(
          result => {resolve(result as Colegio[]);
          },
          err => { reject(err); });
    });
  }

  AgregarColegioDirector(usuario: Usuario, colegio: Colegio) {
    let urlAux = this.url + "/usuariomaster/agregarBDColegioDirector";
    let body = {
      "nombreColegio": colegio.Nombre,
      "nombres": usuario.Nombres,
      "primerApellido": usuario.PrimerApellido,
      "segundoApellido": usuario.SegundoApellido,
      "ci":usuario.Ci,
      "telefono": usuario.Telefono,
      "idUsuarioVerificar": sessionStorage.getItem('user')
    };
    return this.RetornarPromesa(urlAux, body);
  }

  ModificarColegio(colegio: Colegio) {
    let urlAux = this.url + "/colegio/modificarColegio";
    let body = {
      "idColegio": colegio.IdColegio,
      "nombreColegio": colegio.Nombre,
      "idUsuarioVerificar": sessionStorage.getItem('user')
    };
    return this.RetornarPromesa(urlAux, body);
  }

  HabilitarColegio(idColegio: number) {
    let urlAux = this.url + "/colegio/colegioHabilitar";
    let body = {
      "idColegio": idColegio
    };
    return this.RetornarPromesa(urlAux, body);
  }

  DeshabilitarColegio(idColegio: number) {
    let urlAux = this.url + "/colegio/colegioDeshabilitar";
    let body = {
      "idColegio": idColegio
    };
    return this.RetornarPromesa(urlAux, body);
  }

  GetGestiones(idColegio: number) {
    let urlAux = this.url+"/gestion/getGestionesColegio";
    let body = {
      "idColegio": idColegio
    };
    return new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             resolve(res);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }
  GetIdColegio(idColegio: number) {
    let urlAux = this.url+"/colegio/selectIdColegio";
    let body = {
      "idColegio": idColegio
    };
    return new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             resolve(res);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }
  GetDirectorColegio(idColegio: number) {
    let urlAux = this.url+"/usuario/selectDirectorColegio";
    let body = {
      "idColegio": idColegio
    };
    return new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             resolve(res);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }


  AgregarGestionActiva(anio: number, colegio: Colegio) {
    let urlAux = this.url + "/gestion/agregarGestionActiva";
    let body = {
      "idColegio": colegio.IdColegio,
      "anio": anio
    };
    return this.RetornarPromesa(urlAux, body);
  }

  FinalizarGestionActiva(idGestion: number) {
    let urlAux = this.url + "gestion/FinalizarGestionActiva";
    let body = {
      "idGestion": idGestion
    };
    return this.RetornarPromesa(urlAux, body);
  }

  RetornarPromesa(urlAux: string, body: any){
    return new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             resolve(res.mensaje);
           },
           msg => {
             reject(msg);
           }
         );
     });

  }
}
