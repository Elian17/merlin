import { Usuario } from './../Clases/usuario';
import { Sesion } from 'src/app/Clases/sesion';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpParams } from '@angular/common/http';
// import 'rxjs/add/operator/map';
import { map } from "rxjs/operators";
import { SessionStorageService } from 'ngx-webstorage';
import { Observable } from 'rxjs';
import { Curso } from '../Clases/curso';
import { Colegio } from '../Clases/colegio';
import { Materia } from '../Clases/materia';


@Injectable()

export class TutorService {
  private url: string;
  httpOptions = {
      headers: new HttpHeaders({
          'Content-Type': 'application/json'
      })
  }

  constructor(
    private _http: HttpClient,
  ) {
      this.url = "http://trial-bin.000webhostapp.com/colegio/index.php";
  }

  GetEstudantesTutor() {
    return new Promise((resolve, reject) => {
      let urlAux = this.url+"/tutor/getEstudiantesTutor";
      this._http.get(urlAux)
        .map(res => JSON.parse(JSON.stringify(res)))
        .subscribe(
          result => {resolve(result);
          },
          err => { reject(err); });
    });
  }

  GetNotasEstudianteGestion(idEstudiante: number, idGestion: number) {
    let urlAux = this.url+"/tutor/getNotasEstudianteGestion";
    const body = {
      'idEstudiante': idEstudiante,
      'idGestion': idGestion
    };
    return  new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             resolve(res);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }

  RetornarPromesa(urlAux: string, body: any){
    return  new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             resolve(res.mensaje);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }
}
