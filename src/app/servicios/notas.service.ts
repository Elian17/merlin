import { Usuario } from './../Clases/usuario';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpParams } from '@angular/common/http';
// import 'rxjs/add/operator/map';
import { map } from "rxjs/operators";
import { SessionStorageService } from 'ngx-webstorage';
import { Observable } from 'rxjs';
import { Curso } from '../Clases/curso';
import { Colegio } from '../Clases/colegio';
import { Materia } from '../Clases/materia';

@Injectable()
export class NotasService {
  private url: string;
  httpOptions = {
      headers: new HttpHeaders({
          'Content-Type': 'application/json'
      })
  }

  constructor(
    private _http: HttpClient,
    private sessionStorageService: SessionStorageService,
  ) {
      this.url = "http://trial-bin.000webhostapp.com/colegio/index.php";
  }

  AgregarNuevaNota(IdCurso: number,
                   IdMateria: number,
                   Descripcion: string,
                   Tipo: number,
                   IdCategoriaNotas: number,
                   Dia: string,
                   Mes: string) {
    let urlAux = this.url+"/nota/agregarNotasMateria";
    const body = {
      "idColegio":sessionStorage.getItem('colegio'),
      "idCategoriaNotas":IdCategoriaNotas,
      "dia": Dia,
      "mes": Mes,
      "idMateria":IdMateria,
      "idCurso":IdCurso,
      "nota":1,
      "descripcion":Descripcion,
      "tipo":Tipo
    };
    return this.RetornarPromesa(urlAux, body);
  }

  GetCategoriasNotas() {
    let urlAux = this.url+"/categorianota/getCategoriaNota";
    const body = {
      "idColegio":sessionStorage.getItem('colegio')
    };
    return this.RetornarPromesa(urlAux, body);
  }
  GetEstudianteTutor(idTutor:number) {
    let urlAux = this.url+"/nota/getTutorEstudiante";
    const body = {
      "idColegio":sessionStorage.getItem('colegio'),
      "idTutor":idTutor
    };
    return this.RetornarPromesa(urlAux, body);
  }
  GetMateriasEstudiante(idEstudiante:number) {
    let urlAux = this.url+"/asistencia/materiasDelEstudiante";
    const body = {
      "idColegio":sessionStorage.getItem('colegio'),
      "idEstudiante":idEstudiante
    };
    return this.RetornarPromesa(urlAux, body);
  }


  UpdateNotaEstudiante(idNota:number, nuevanota: number) {
    let urlAux = this.url+"/nota/updateNotaEstudiante";
    const body = {
      "idNota": idNota,
      "nuevaNota": nuevanota
    };
    return this.RetornarPromesaMensaje(urlAux, body);
  }


  AnularNota(idNota:number) {
    let urlAux = this.url+"/nota/anularNotaEstudiante";
    const body = {
      "idNota": idNota
    };
    return this.RetornarPromesa(urlAux, body);
  }

  GetEstudiantesNotas(idMateria:number, idCurso:number) {
    let urlAux = this.url+"/nota/getEstudiantesNotas";
    const body = {
      "idColegio":sessionStorage.getItem('colegio'),
      "idMateria":idMateria,
      "idCurso":idCurso
    };
    return this.RetornarPromesa(urlAux, body);
  }
  GetNotasIndividual(idEstudiante:number,idMateria:number, idCate:number) {
    let urlAux = this.url+"/nota/notasDelEstudiante";
    const body = {
      "idColegio":sessionStorage.getItem('colegio'),
      "idMateria":idMateria,
      "idEstudiante":idEstudiante,
      "categoria":idCate
    };
    return this.RetornarPromesa(urlAux, body);
  }

  RetornarPromesa(urlAux: string, body: any){
    return  new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             console.log(res);
             resolve(res);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }

  RetornarPromesaMensaje(urlAux: string, body: any){
    return  new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             resolve(res.Mensaje);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }
}
