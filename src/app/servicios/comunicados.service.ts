import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ComunicadosService {
  private url: string;
  httpOptions = {
      headers: new HttpHeaders({
          'Content-Type': 'application/json'
      })
  }
  constructor(private _http: HttpClient,) {
    this.url = "http://trial-bin.000webhostapp.com/colegio/index.php";
   }
   GetComunicados(idColegio: number,idUsuario:number) {
    const urlAux = this.url + '/comunicado/getComunicado';
    const body = {
      'idColegio': idColegio,
      'idUsuario':idUsuario
    };
    return new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             resolve(res);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }
  AgregarComunicado(idUsuario:number,idColegio:number,  titulo: string,mensaje: string,tipo: string, tipoAgregar: [],
    idCurso: number,Receptores: []){
    const urlAux = this.url + '/comunicado/agregarComunicado';
    const body = {
      'idUsuario': idUsuario,
      'idColegio': idColegio,
      'titulo': titulo,
      'mensaje': mensaje,
      'tipo': tipo,
      'tipoAgregar': tipoAgregar,
      'idCurso': idCurso,
      'receptores':Receptores
    };
    return this.RetornarPromesa(urlAux, body);
  }
  RetornarPromesa(urlAux: string, body: any){
    return  new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             resolve(res.mensaje);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }
  EliminarComunicado(idComunicado: number){
    const urlAux = this.url + '/comunicado/eliminarComunicado';
    const body = {
      'idComunicado': idComunicado
    };
    return this.RetornarPromesa(urlAux, body);
  }
  cambioEstadoComunicado(idComunicado: number,idUsuarioReceptor:number){
    const urlAux = this.url + '/comunicado/eliminarComunicado';
    const body = {
      'idComunicado': idComunicado,
      'idUsuarioReceptor':idUsuarioReceptor
    };
    return this.RetornarPromesa(urlAux, body);
  }
  ModificarComunicado(idComunicado:number,titulo:string,mensaje:string,tipo:string){
    const urlAux = this.url + '/comunicado/updateComunicado';
    const body = {

      'idComunicado': idComunicado,
      'titulo': titulo,
      'mensaje': mensaje,
      'tipo': tipo
        };
    return this.RetornarPromesa(urlAux, body);
  }
  GetComunicadosUsuario(idUsuario:number) {
    const urlAux = this.url + '/comunicado/verComunicadosUsuario';
    const body = {
      'idUsuario':idUsuario
    };
    return new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             resolve(res);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }
  GetComunicadosCantidad(idUsuario:number) {
    const urlAux = this.url + '/comunicado/getCantidadComunidad';
    const body = {
      'idUsuario':idUsuario
    };
    return new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             resolve(res);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }
}
