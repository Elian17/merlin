import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Curso } from '../Clases/curso';


@Injectable()

export class CursosService {
  private url: string;
  httpOptions = {
      headers: new HttpHeaders({
          'Content-Type': 'application/json'
      })
  }

  constructor(
    private _http: HttpClient,
  ) {
      this.url = "http://trial-bin.000webhostapp.com/colegio/index.php";
  }

  AgregarCursoColegio(curso: Curso){
    const urlAux = this.url + '/curso/agregarCurso';
    const body = {
      'idColegio': curso.IdColegio,
      'grado': curso.Grado,
      'nivel': curso.Nivel,
      'paralelo': curso.Paralelo
    };
    return this.RetornarPromesa(urlAux, body);
  }
  ModificarCursoColegio(idCurso:number,curso: Curso){
    const urlAux = this.url + '/curso/modificarCurso';
    const body = {

      'idCurso': idCurso,
      'grado': curso.Grado,
      'nivel': curso.Nivel,
      'paralelo': curso.Paralelo
    };
    return this.RetornarPromesa(urlAux, body);
  }

  GetCursosColegio(idColegio: number) {
    const urlAux = this.url + '/curso/getCursoColegio';
    const body = {
      'idColegio': idColegio
    };
    return new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             resolve(res);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }
  GetNivelesCurso(idColegio: number) {
    const urlAux = this.url + '/curso/getNivelColegio';
    const body = {
      'idColegio': idColegio
    };
    return new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             resolve(res);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }
  GetCursoNivel(idColegio: number,nivel:string) {
    const urlAux = this.url + '/curso/getCursoNivel';
    const body = {
      'idColegio': idColegio,
      'nivel':nivel
    };
    return new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             resolve(res);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }

  EliminarCurso(idCurso: number){
    const urlAux = this.url + '/curso/eliminarCurso';
    const body = {
      'idCurso': idCurso
    };
    return this.RetornarPromesa(urlAux, body);
  }
  //
  GetEstudiantesCursoGestionActual(idCurso: number) {//Solo puede haber una gestion activa
    const urlAux = this.url + '/cursos/getEstudiantesCursoGestionActual';
    const body = {
      'idCurso': idCurso
    };
    return  new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             resolve(res);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }

  RetornarPromesa(urlAux: string, body: any){
    return  new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             resolve(res.mensaje);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }
}
