import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpParams } from '@angular/common/http';


@Injectable()

export class EstudianteService {
  private url: string;
  httpOptions = {
      headers: new HttpHeaders({
          'Content-Type': 'application/json'
      })
  }

  constructor(
    private _http: HttpClient,
  ) {
      this.url = "http://trial-bin.000webhostapp.com/colegio/index.php";
  }

  AsignarTutores(idEstudiante: number, tutores: number[]){
    const urlAux = this.url + '/estudiante/agregarTutorEst';
    const body = {
      'tutores': tutores,
      'estudiante': idEstudiante
    };
    return this.RetornarPromesa(urlAux, body);
  }

  QuitarTutor(idEstudiante: number, idTutor: number){
    const urlAux = this.url + '/estudiante/eliminarTutorEst';
    const body = {
      'tutor': idTutor,
      'estudiante': idEstudiante,
    };
    return this.RetornarPromesa(urlAux, body);
  }
  //
  AsignarACursoGestion(idEstudiante: number, idCurso: number, idGestion: number){
    const urlAux = this.url + '/estudiante/asignarCursoGestion';
    const body = {
      'idEstudiante': idEstudiante,
      'idCurso': idCurso,
      'idGestion': idGestion
    };
    return this.RetornarPromesa(urlAux, body);
  }
  
  //
  AbandonoEstuduante(idEstudiante: number, idCurso: number, idGestion: number){//cambia el estado del cursoEstudiante y el estudiante
    const urlAux = this.url + '/estudiante/abandonoEstudiante';
    const body = {
      'idEstudiante': idEstudiante,
      'idCurso': idCurso,
      'idGestion': idGestion
    };
    return this.RetornarPromesa(urlAux, body);
  }

  RetornarPromesa(urlAux: string, body: any){
    return  new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             resolve(res.mensaje);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }
}
