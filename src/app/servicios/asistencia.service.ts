import { Usuario } from './../Clases/usuario';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpParams } from '@angular/common/http';
// import 'rxjs/add/operator/map';
import { map } from "rxjs/operators";
import { SessionStorageService } from 'ngx-webstorage';
import { Observable } from 'rxjs';
import { Curso } from '../Clases/curso';
import { Colegio } from '../Clases/colegio';
import { Materia } from '../Clases/materia';


@Injectable()
export class AsistenciaService {
  private url: string;
  httpOptions = {
      headers: new HttpHeaders({
          'Content-Type': 'application/json'
      })
  }

  constructor(
    private _http: HttpClient,
    private sessionStorageService: SessionStorageService,
  ) {
      this.url = "http://trial-bin.000webhostapp.com/colegio/index.php";
  }

  GetCursosProfesor(idProfesor: number) {
    let urlAux = this.url+"/maestro/getMateriasDocente";
    const body = {
      "idColegio":sessionStorage.getItem('colegio'),
      "idDocente":idProfesor
    };
    return this.RetornarPromesa(urlAux, body);
  }

  GetAsistenciasEstudiantesRango(idMateria: number,
                                 diaInicio: string,
                                 mesInicio: string,
                                 diaFin: string,
                                 mesFin: string) {
    let urlAux = this.url+"/asistencia/getAsistenciaEstudianteFecha";
    const body = {
      "idColegio":sessionStorage.getItem('colegio'),
      "idMateria":idMateria,
      "fechaDiaInicio":diaInicio,
      "fechaMesInicio":mesInicio,
      "fechaDiaFinal":diaFin,
      "fechaMesFinal":mesFin
    };
    console.log(body);
    return this.RetornarPromesa(urlAux, body);
  }

  GetAsistenciasMateriaEstudiantesRango(idEstudiante: number,
    diaInicio: string,
    mesInicio: string,
    diaFin: string,
    mesFin: string) {
let urlAux = this.url+"/asistencia/getAsistenciaPorMateria";
const body = {
"idColegio":sessionStorage.getItem('colegio'),
"idEstudiante":idEstudiante,
"fechaDiaInicio":diaInicio,
"fechaMesInicio":mesInicio,
"fechaDiaFinal":diaFin,
"fechaMesFinal":mesFin
};
console.log(body);
return this.RetornarPromesa(urlAux, body);
}



  UpdateAsistencia(idAsistencia: number, estado: number){
    let urlAux = this.url+"/asistencia/updateAsistencia";
    const body = {
      "idAsistencia":idAsistencia,
      "estado":estado
    };
    return this.RetornarPromesa(urlAux, body);
  }

  GetIdFechaHoy(dia: string, mes: string){
    let urlAux = this.url+"/asistencia/selectIdFechas";
    const body = {
      "dia":dia,
      "mes":mes
    };
    return this.RetornarPromesa(urlAux, body);
  }

  AgregarAsistencia(idMateria: number, dia: number, mes: number, idEstudiante: number, estado: number) {
    let urlAux = this.url+"/asistencia/agregarAsistencia";
    const body = {
      "idColegio": sessionStorage.getItem('colegio'),
      "idMateria": idMateria,
      "dia": dia,
      "mes": mes,
      "idEstudiante": idEstudiante,
      "estado": estado
    };
    console.log(body);
    return this.RetornarPromesa(urlAux, body);
  }

  TomarAsistencia(idMateria: number, dia: number, mes: number, idCurso: number) {
    let urlAux = this.url+"/asistencia/agregar";
    const body = {
      "idColegio":sessionStorage.getItem('colegio'),
      "idMateria":idMateria,
      "dia":dia,
      "mes":mes,
      "idCurso":idCurso
    };
    console.log(body);
    return this.RetornarPromesa(urlAux, body);
  }

  GetEstudiantesCurso(idCurso: number) {
    let urlAux = this.url+"/asistencia/getCursosDiaProfesor";
    const body = {
      'idCurso': idCurso
    };
    return this.RetornarPromesa(urlAux, body);
  }

  AgregarAsistenciaIndividual(idEstudiante: number, idCurso: number) {//agrega la asistencia de la fecha actual
    let urlAux = this.url+"/asistencia/agregarAsistenciaIndividual";
    const body = {
      'idEstudiante': idEstudiante,
      'idCurso': idCurso
    };
    return this.RetornarPromesaMensaje(urlAux, body);
  }

  GetAsisenciasCursoUltimosNDias(idEstudiante: number, idCurso: number, numeroDias: number) {
    let urlAux = this.url+"/asistencia/getAsisenciasCursoUltimosNDias";
    const body = {
      'idEstudiante': idEstudiante,
      'idCurso': idCurso,
      'numeroDias': numeroDias
    };
    return this.RetornarPromesa(urlAux, body);
  }

  GetAsistenciasCursosRangoFechas(idEstudiante: number, idCurso: number, fechaInicio: Date, fechaFin: Date) {
    let urlAux = this.url+"/asistencia/getAsistenciasCursosRangoFechas";
    const body = {
      'idEstudiante': idEstudiante,
      'idCurso': idCurso,
      'fechaInicio': fechaInicio,
      'fechaFin': fechaFin
    };
    return this.RetornarPromesa(urlAux, body);
  }

  RetornarPromesa(urlAux: string, body: any){
    return  new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             console.log(res);
             resolve(res);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }

  RetornarPromesaMensaje(urlAux: string, body: any){
    return  new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             resolve(res.Mensaje);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }
}
