import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Periodo } from '../Clases/periodo';

@Injectable({
  providedIn: 'root'
})
export class PeriodosService {

  private url: string;
  httpOptions = {
      headers: new HttpHeaders({
          'Content-Type': 'application/json'
      })
  }
  constructor(private _http: HttpClient,) { 
    this.url = "http://trial-bin.000webhostapp.com/colegio/index.php";
  }

  GetPeriodosColegio(idColegio: number) {
    const urlAux = this.url + '/periodo/getPeriodosColegio';
    const body = {
      'idColegio': idColegio
    };
    return new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             resolve(res);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }

  AgregarPeriodoColegio(periodo: Periodo){
    const urlAux = this.url + '/periodo/agregarPeriodo';
    const body = {
      'periodo': [
        { 'horaInicio':periodo.HoraInicio,
          'horaFin':periodo.HoraFin
        }
      ],
      'idColegio': periodo.IdColegio,
      'idUsuarioVerificar': sessionStorage.getItem('user')
    };
    return this.RetornarPromesa(urlAux, body);
  }

  EliminarPeriodo(idPeriodo: number){
    const urlAux = this.url + '/periodo/eliminarPeriodo';
    const body = {
      'idPeriodo': idPeriodo
    };
    return this.RetornarPromesa(urlAux, body);
  }

  RetornarPromesa(urlAux: string, body: any){
    return  new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             resolve(res.mensaje);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }

}
