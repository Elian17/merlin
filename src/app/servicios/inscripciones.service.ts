import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class InscripcionesService {
  private url: string;
  httpOptions = {
      headers: new HttpHeaders({
          'Content-Type': 'application/json'
      })
  }

  constructor(private _http: HttpClient,) { 
    this.url = "http://trial-bin.000webhostapp.com/colegio/index.php";
  
  }
  GetCursoEstudiante(idColegio: number,$idCurso:number) {
    const urlAux = this.url + '/inscripciones/getEstudianteCurso';
    const body = {
      'idColegio': idColegio,
      'idCurso':$idCurso
    };
    return new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             resolve(res);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }
  HabilitarInscripcionEstudiante(idCursoEstudiante: number){
    const urlAux = this.url + '/inscripciones/HabilitarInscripcionEstudiante';
    const body = {
      'idCursoEstudiante': idCursoEstudiante,
      
    };
    return this.RetornarPromesa(urlAux, body);
  }
  AgregarInscripcionEstudiante(estudiante:number[],idCurso: number,idColegio:number){
    const urlAux = this.url + '/inscripciones/agregarInscripcionEstudiante';
    const body = {
      'idCurso': idCurso,
      'estudiante':estudiante,
      'idColegio':idColegio
      
    };
    return this.RetornarPromesa(urlAux, body);
  }
  InhabilitarInscripcionEstudiante(idCursoEstudiante: number){
    const urlAux = this.url + '/inscripciones/InhabilitarInscripcionEstudiante';
    const body = {
      'idCursoEstudiante': idCursoEstudiante,
      
    };
    return this.RetornarPromesa(urlAux, body);
  }


  RetornarPromesa(urlAux: string, body: any){
    return  new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             resolve(res.mensaje);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }
}
