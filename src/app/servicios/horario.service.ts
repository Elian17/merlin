import { Usuario } from './../Clases/usuario';
import { Sesion } from 'src/app/Clases/sesion';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpParams } from '@angular/common/http';
// import 'rxjs/add/operator/map';
import { map } from "rxjs/operators";
import { SessionStorageService } from 'ngx-webstorage';
import { Observable } from 'rxjs';
import { Curso } from '../Clases/curso';
import { Colegio } from '../Clases/colegio';
import { Materia } from '../Clases/materia';
import { Horario } from '../Clases/horario';


@Injectable()

export class HorarioService {
  private url: string;
  httpOptions = {
      headers: new HttpHeaders({
          'Content-Type': 'application/json'
      })
  }

  constructor(
    private _http: HttpClient,
  ) {
      this.url = "http://trial-bin.000webhostapp.com/colegio/index.php";
  }
  //--//--//--//!!

  AgregarHorarioCurso(horario:Horario){
    const urlAux = this.url + '/horario/agregarHorario';
    const body = {
      'dia': horario.Dia,
      'idMateria':horario.IdMateria,
      'idCurso':horario.IdCurso,
      'idColegio': horario.IdColegio,
      'idPeriodo': horario.IdPeriodo
    };
    return this.RetornarPromesa(urlAux, body);
  }

  GetHorariosCurso(idCurso: number,idColegio:number) {
    let urlAux = this.url+"/horario/getHorariosCurso";
    const body = {
      'idColegio':idColegio,
      'idCurso': idCurso
    };
    return this.RetornarPromesa(urlAux, body);
  }

  EliminarHorario(idHorario: number) {
    let urlAux = this.url+"/horario/eliminarHorario";
    const body = {
      'idHorario':idHorario
    };
    return this.RetornarPromesa(urlAux, body);
  }


  GetHorarioEstudiante(idEstudiante: number) {//Devuelve el horario del curso en el que está el estudiante en la gestion actual
    let urlAux = this.url+"/horario/getHorarioEstudiante";
    const body = {
      'idEstudiante': idEstudiante
    };
    return this.RetornarPromesa(urlAux, body);
  }

  GetHorarioCurso(idCurso: number) {
    let urlAux = this.url+"/horario/getHorarioCurso";
    const body = {
      'idCurso': idCurso
    };
    return this.RetornarPromesa(urlAux, body);
  }

  GetHorarioProfesor(idProfesor: number) {//Devuelve los cursos y la hora en que da clases el profesor de toda la semana
    let urlAux = this.url+"/horario/getHorarioProfesor";
    const body = {
      'idProfesor': idProfesor
    };
    return this.RetornarPromesa(urlAux, body);
  }

  RetornarPromesa(urlAux: string, body: any){
    return  new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             resolve(res);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }

  RetornarPromesaMensaje(urlAux: string, body: any){
    return  new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             resolve(res.Mensaje);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }
}
