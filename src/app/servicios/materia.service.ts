import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Materia } from '../Clases/materia';


@Injectable()

export class MateriaService {
  private url: string;
  httpOptions = {
      headers: new HttpHeaders({
          'Content-Type': 'application/json'
      })
  }

  constructor(
    private _http: HttpClient,
  ) {
      this.url = "http://trial-bin.000webhostapp.com/colegio/index.php";
  }

  AgregarMateria(materia: Materia[],idColegio:number){
    const urlAux = this.url + '/materia/agregarMateria';
    const body = {
      'materia': materia,
	    "idColegio":idColegio
    };
    return this.RetornarPromesa(urlAux, body);
  }

  HabilitarMateria(idMateria: number){
    const urlAux = this.url + '/materia/HabilitarMateria';
    const body = {
      'idMateria': idMateria
    };
    return this.RetornarPromesa(urlAux, body);
  }

  InhabilitarMateria(idMateria: number){
    const urlAux = this.url + '/materia/InhabilitarMateria';
    const body = {
      'idMateria': idMateria
    };
    return this.RetornarPromesa(urlAux, body);
  }

  GetMateriasColegio(idColegio: number) {
    const urlAux = this.url + '/materia/MateriasColegio';
    const body = {
      'idColegio': idColegio
    };
    return  new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             resolve(res);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }
  GetMateriasProfesor(idColegio: number,idCurso:number) {
    const urlAux = this.url + '/materia/SelectMateriaProfe';
    const body = {
      'idColegio': idColegio,
      'idCurso':idCurso
    };
    return  new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             resolve(res);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }
  GetMateriasCursoColegio(idColegio: number, idCurso: number) {
    const urlAux = this.url + '/materia/materiasCursoColegio';
    const body = {
      'idColegio': idColegio,
      'idCurso': idCurso
    };
    return  new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             resolve(res);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }

  RetornarPromesa(urlAux: string, body: any){
    return  new Promise((resolve, reject) => {
      this._http.post(urlAux, JSON.stringify(body)).map(res => JSON.parse(JSON.stringify(res))).toPromise()
         .then(
           res => {
             resolve(res.mensaje);
           },
           msg => {
             reject(msg);
           }
         );
     });
  }
}
