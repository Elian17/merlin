import { Component, OnInit, Input } from '@angular/core';
import { NgxMaterialTimepickerTheme } from 'ngx-material-timepicker';
import { AsistenciaService } from 'src/app/servicios/asistencia.service';
import { formatDate } from '@angular/common';
import { SessionStorageService } from 'ngx-webstorage';

@Component({
  selector: 'app-verasistencia',
  templateUrl: './verasistencia.component.html',
  styleUrls: ['./verasistencia.component.scss']
})
export class VerasistenciaComponent implements OnInit {

  constructor(public asisteciaService: AsistenciaService,private sessionStorage:SessionStorageService) {
    this.get();
   }
  @Input() curso: any;
  @Input() lista: any;
  @Input() asistencias: any;
  diaHoy: number;
  mesHoy: number;
  @Input() displayedColumns: any[]=[];

  antiguo: any;

  rango = {
    start: new Date(),
    end: new Date(),
  };

  darkTheme: NgxMaterialTimepickerTheme = {
    container: {
        bodyBackgroundColor: '#323259',
        buttonColor: '#fff'
    },
    dial: {
        dialBackgroundColor: '#1B1B38',
    },
    clockFace: {
        clockFaceBackgroundColor: '#1B1B38',
        clockHandColor: '#AF87FF',
        clockFaceTimeInactiveColor: '#fff'
    }
};
  ngOnInit() {
   
  }
  agregarMesLista(mes) {
    switch (mes) {
      case 1:
        this.displayedColumns.push({nMes: 1, mes: 'Enero', nFechas: 1});
        break;
      case 2:
          this.displayedColumns.push({nMes: 2, mes: 'Febrero', nFechas: 1});
          break;
      case 3:
          this.displayedColumns.push({nMes: 3, mes: 'Marzo', nFechas: 1});
          break;
      case 4:
          this.displayedColumns.push({nMes: 4, mes: 'Abril', nFechas: 1});
          break;
      case 5:
          this.displayedColumns.push({nMes: 5, mes: 'Mayo', nFechas: 1});
          break;
      case 6:
          this.displayedColumns.push({nMes: 6, mes: 'Junio', nFechas: 1});
          break;
      case 7:
          this.displayedColumns.push({nMes: 7, mes: 'Julio', nFechas: 1});
          break;
      case 8:
          this.displayedColumns.push({nMes: 8, mes: 'Agosto', nFechas: 1});
          break;
      case 9:
          this.displayedColumns.push({nMes: 9, mes: 'Septiembre', nFechas: 1});
          break;
      case 10:
          this.displayedColumns.push({nMes: 10, mes: 'Octubre', nFechas: 1});
          break;
      case 11:
          this.displayedColumns.push({nMes: 11, mes: 'Noviembre', nFechas: 1});
          break;
      case 12:
          this.displayedColumns.push({nMes: 12, mes: 'Diciembre', nFechas: 1});
          break;

      default:
        break;
    }
  }
  async rangoFechas() {
    this.asistencias = await this.asisteciaService.GetAsistenciasMateriaEstudiantesRango(parseInt(sessionStorage.getItem('user')),
      formatDate(this.rango.start, 'yyyy/MM/dd', 'en').split('/')[2],
      formatDate(this.rango.start, 'yyyy/MM/dd', 'en').split('/')[1],
      formatDate(this.rango.end, 'yyyy/MM/dd', 'en').split('/')[2],
      formatDate(this.rango.end, 'yyyy/MM/dd', 'en').split('/')[1]);
      console.log(this.asistencias);
      console.log("entro rango de fechas");
  }
  async get() {
    this.asistencias = await this.asisteciaService.GetAsistenciasMateriaEstudiantesRango(parseInt(sessionStorage.getItem('user')), "0","0","0","0");
    console.log(this.asistencias);
    console.log("pruebas");
    let duplicado = false;
    let mesEnLista;
    this.asistencias.fechas.forEach(fecha => {
        mesEnLista = parseInt(fecha.mes);
        for (let i = 0; i < this.displayedColumns.length; i++) {
          if (mesEnLista == parseInt(this.displayedColumns[i].nMes)) {
            duplicado = true;
            this.displayedColumns[i].nFechas++;
          }
        }
        if (!duplicado) {
            this.agregarMesLista(mesEnLista);
            duplicado = false;
        }
        duplicado = false;

      });
    this.displayedColumns.sort(function(a,b){return a.nMes - b.nMes;});
  this.asistencias.fechas.sort(function(a,b){return a.idFecha - b.idFecha;});
  //this.asistencias.fechas.fechas.sort(function(a,b){return a.mes - b.mes;});

  }
}
