import { Component, OnInit, ViewChild } from '@angular/core';
import { NotasService } from 'src/app/servicios/notas.service';
import { SessionStorageService } from 'ngx-webstorage';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';

@Component({
  selector: 'app-vernotas',
  templateUrl: './vernotas.component.html',
  styleUrls: ['./vernotas.component.scss']
})
export class VernotasComponent implements OnInit {
  Categoria:any;
  Materia:any;
  idCategoria:number;
  idMateria:number;
  Notas:any;
  displayedColumns: string[] = ['Materia','Descripcion','Nota', 'Tipo', 'Categoria','Fecha'];
  dataSource = this.Notas;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort,{static: false}) sort: MatSort;
  constructor(private notaService:NotasService,private sessionStorage:SessionStorageService) { }

  async ngOnInit() {
    this.Categoria=await this.notaService.GetCategoriasNotas();
    this.Materia = await this.notaService.GetMateriasEstudiante(parseInt(sessionStorage.getItem('user')));
  }
  cargarNotas(value:any):void {
    console.log('Selected value is:sdsda ', value);
    
    if(value !="undefined"){
     
      this.cargarDatos(value);
    }

  }
  async cargarDatos(idMateria:number){
    this.Notas= await this.notaService.GetNotasIndividual(parseInt(sessionStorage.getItem('user')),idMateria,this.idCategoria);
     this.dataSource = new MatTableDataSource(this.Notas);
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
