import { Component, OnInit } from '@angular/core';
import { AsistenciaService } from 'src/app/servicios/asistencia.service';

@Component({
  selector: 'app-notas',
  templateUrl: './notas.component.html',
  styleUrls: ['./notas.component.scss']
})
export class NotasComponent implements OnInit {

  meses: any[] = [];
  cursos: any[] = [];
  curso: any;
  displayedColumns: any[] = [];
  constructor(private asistenciaService: AsistenciaService) { }

  async ngOnInit() {
    this.cursos = await this.asistenciaService.GetCursosProfesor(parseInt(sessionStorage.getItem('user'))) as any[];
  }
  seleccionCurso(curso: any) {
    this.curso = curso;
  }

volverCursos() {
    this.curso = undefined;
    this.displayedColumns=[];
    this.meses=[];
  }

}
