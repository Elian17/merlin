import { Component, OnInit } from '@angular/core';
import { AsistenciaService } from 'src/app/servicios/asistencia.service';

@Component({
  selector: 'app-horarios',
  templateUrl: './horarios.component.html',
  styleUrls: ['./horarios.component.scss']
})
export class HorariosComponent implements OnInit {

  meses: any[] = [];
  cursos: any[] = [];
  curso: any;
  asistencias: any;
  displayedColumns: any[] = [];
  constructor(private asistenciaService: AsistenciaService) { }

  async ngOnInit() {
    this.cursos = await this.asistenciaService.GetCursosProfesor(parseInt(sessionStorage.getItem('user'))) as any[];
  }
  seleccionCurso(curso: any) {
    this.curso = curso;
  }

volverCursos() {
    this.curso = undefined;
    this.displayedColumns=[];
    this.meses=[];
    this.asistencias=undefined;
  }

}
