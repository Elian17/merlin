import { NotasService } from './../../servicios/notas.service';
import { NuevaNotaComponent } from './../nueva-nota/nueva-nota.component';
import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { formatDate } from '@angular/common';

export interface DialogTarea {
  bimestre: number;
  descripcion: string;
  tipoNota: number;
  fechaNota: Date;
}
@Component({
  selector: 'app-vista-notas',
  templateUrl: './vista-notas.component.html',
  styleUrls: ['./vista-notas.component.scss']
})
export class VistaNotasComponent implements OnInit {


  constructor(public dialog: MatDialog, public notasService: NotasService) { }

  @Input() curso: any;
  @Input() lista: any;

  @Input() displayedColumns: any[];

  bimestre: number;
  descripcion: string;
  tipoNota: number;
  fechaNota: Date;

  categoriasNotas: any;

  dimensionesEvaluacion = [
  [
      {
        dimension: 'Ser',
        columnas: 0
      },
      {
        dimension: 'Saber',
        columnas: 0
      },
      {
        dimension: 'Hacer',
        columnas: 0
      },
      {
        dimension: 'Decidir',
        columnas: 0
      }
    ],
    [
      {
        dimension: 'Ser',
        columnas: 0
      },
      {
        dimension: 'Saber',
        columnas: 0
      },
      {
        dimension: 'Hacer',
        columnas: 0
      },
      {
        dimension: 'Decidir',
        columnas: 0
      }
    ],
    [
      {
        dimension: 'Ser',
        columnas: 0
      },
      {
        dimension: 'Saber',
        columnas: 0
      },
      {
        dimension: 'Hacer',
        columnas: 0
      },
      {
        dimension: 'Decidir',
        columnas: 0
      }
    ],
    [
      {
        dimension: 'Ser',
        columnas: 0
      },
      {
        dimension: 'Saber',
        columnas: 0
      },
      {
        dimension: 'Hacer',
        columnas: 0
      },
      {
        dimension: 'Decidir',
        columnas: 0
      }
    ],
    [
      {
        dimension: 'Ser',
        columnas: 0
      },
      {
        dimension: 'Saber',
        columnas: 0
      },
      {
        dimension: 'Hacer',
        columnas: 0
      },
      {
        dimension: 'Decidir',
        columnas: 0
      }
    ],
    ];

  server: any;




  notaAnterior;

  async ngOnInit() {
    await this.CargarNotas();
    await this.GetCategoriasNotas();
    await this.columnasDimensiones();
    await this.ordenarNotas();
  }

  async ordenarNotas(){
    var x=0;
    this.server.estudiantes.forEach(est => {
      est.notas.forEach(nota => {
        for (let i = 0; i < this.server.notas.length; i++) {
          if (parseInt(nota.idNota)==parseInt(this.server.notas[i].idNota)+x) {
            nota.idFecha=this.server.notas[i].idFecha;
            nota.tipo = this.server.notas[i].tipo;
            nota.idCategoriaNotas = this.server.notas[i].idCategoriaNotas;
            break;

          }
        }

      });
      x++;
    });
    await console.log(this.server);
    this.server.estudiantes.forEach(est => {
      est.notas.sort(
        function(a, b) {return (a.idFecha - b.idFecha); }
      );
    });
    this.server.estudiantes.forEach(est => {
      est.notas.sort(
        function(a, b) {return (a.tipo - b.tipo); }
      );
    });
    this.server.estudiantes.forEach(est => {
      est.notas.sort(
        function(a, b) {return (a.idCategoriaNotas - b.idCategoriaNotas); }
      );
    });
    await console.log(this.server);
  }


  async CargarNotas(){
    this.server = await this.notasService.GetEstudiantesNotas(this.curso.idMateria, this.curso.idCurso);
    this.server.notas.sort(
      function(a, b) {return (a.tipo - b.tipo); }
    );
    this.server.notas.sort(
      function(a, b) {return (a.idCategoriaNotas - b.idCategoriaNotas); }
    );
  }

  async GetCategoriasNotas(){

    this.categoriasNotas = await this.notasService.GetCategoriasNotas();


    this.categoriasNotas.forEach(bimestre => {
      bimestre.columnas=0;
    });

    this.categoriasNotas.sort(
      function(a, b) {return a.idCategoria - b.idCategoria; }
      );

    this.server.notas.forEach(nota => {
      this.categoriasNotas[nota.idCategoriaNotas - 1].columnas++;
    });
  }

  columnasDimensiones() {
    const bim = 0;
    this.server.notas.forEach(fecha => {
      this.dimensionesEvaluacion[fecha.idCategoriaNotas - 1][fecha.tipo - 1].columnas++;
    });
  }



  notaAntigua(idEstudiante: any, idNota: any, nota: any, input:any){
    input.value='';
    this.notaAnterior = nota;
  }

  cambioNotaInterfaz(nuevaNota: any, input: any) {
    if (!isNaN(nuevaNota)) {
      if (parseInt(nuevaNota) > 50) {
        input.style = 'border: 1px solid #00D68F;';
      } else {
        input.style = 'border: 1px solid #FF3D71;';
      }
    }
    // else{
    //   if (this.notaAnterior!=null) {
    //     input.value=this.notaAnterior;
    //     this.cambioNotaInterfaz(nuevaNota, input);
    //   }
    // }


  }

  cambioNotaServidor(idEstudiante: any, idNota: any, nuevaNota: any, input: any) {
    if (!isNaN(nuevaNota)) {
      if (nuevaNota>0 && nuevaNota<=100) {
        console.log(idNota +" "+ idEstudiante);
        this.notasService.UpdateNotaEstudiante(idNota as number, nuevaNota as number).then(
          result =>{
            if (result==1) {
              input.value = nuevaNota;
              this.cambioNotaInterfaz(nuevaNota, input);
            }
          },
          error => {
              console.log(error);
          }
        )

      }
      else{
        input.value=this.notaAnterior;
        this.cambioNotaInterfaz(this.notaAnterior, input);
      }
    }
    else{
      input.value=this.notaAnterior;
      this.cambioNotaInterfaz(this.notaAnterior, input);
    }

  }
  cargarNuevaNota(descripcion: string, tipoNota: number, bimestre: number, fecha: Date ){
    console.log("asd");
    console.log(descripcion);
    console.log(tipoNota);
    console.log(bimestre);
    console.log(fecha);
  }




  NuevaTarea(): void {
    const dialogRef = this.dialog.open(NuevaNotaComponent, {
          width: '500px',
          data: { bimestre: this.bimestre,
                  descripcion: this.descripcion,
                  tipoNota: this.tipoNota,
                  fecha: this.fechaNota }
        });

    dialogRef.afterClosed().subscribe(async result => {
        if (result.descripcion !==null &&
            result.tipoNota !==null &&
            result.bimestre !==null &&
            result.fecha !== null) {
          await this.notasService.AgregarNuevaNota(this.curso.idCurso as number,
            this.curso.idMateria,
            result.descripcion,
            result.tipoNota,
            result.bimestre,
            formatDate(result.fecha, 'yyyy/MM/dd', 'en').split('/')[2],
            formatDate(result.fecha, 'yyyy/MM/dd', 'en').split('/')[1]).then(
              res => {
                console.log(res);
                if (res!==null && res!==undefined) {
                  this.cargarNuevaNota(result.descripcion,
                                       result.tipoNota,
                                       result.bimestre,
                                       result.fecha);
                  alert("Nota creada con éxito");
                }
                else{
                }
              },
              error => {
                console.log(error);
              }
            );
        }
         });
  }

}
