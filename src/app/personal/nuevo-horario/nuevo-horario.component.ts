import { Component, OnInit, Inject} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogData } from 'src/app/personal/curso-horario/curso-horario.component';

@Component({
  selector: 'app-nuevo-horario',
  templateUrl: './nuevo-horario.component.html',
  styleUrls: ['./nuevo-horario.component.scss']
})
export class NuevoHorarioComponent implements OnInit {

  idPeriodo: number;
  idMateria: number;
  
  periodos: [];
  materias: [];

  constructor(  public dialogRef:MatDialogRef<NuevoHorarioComponent>,
                @Inject(MAT_DIALOG_DATA) public data:DialogData) { }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
