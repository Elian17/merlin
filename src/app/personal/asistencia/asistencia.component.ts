import { PersonalInicioComponent } from './../../usuarios/personal-inicio/personal-inicio.component';
import { AsistenciaService } from './../../servicios/asistencia.service';
import { Component, OnInit, Input } from '@angular/core';
import { NgxMaterialTimepickerTheme } from 'ngx-material-timepicker';
import {formatDate} from '@angular/common';
@Component({
  selector: 'app-asistencia',
  templateUrl: './asistencia.component.html',
  styleUrls: ['./asistencia.component.scss']
})
export class AsistenciaComponent implements OnInit {
  constructor(public asisteciaService: AsistenciaService,
              public personalInicioComponent: PersonalInicioComponent) { }

  tomarAsistencia = false;
  mesTomarAsistencia = [];

  @Input() curso: any;
  @Input() lista: any;
  @Input() asistencias: any;
  diaHoy: number;
  mesHoy: number;


  @Input() displayedColumns: any[];

  antiguo: any;

  rango = {
    start: new Date(),
    end: new Date(),
  };

  darkTheme: NgxMaterialTimepickerTheme = {
    container: {
        bodyBackgroundColor: '#323259',
        buttonColor: '#fff'
    },
    dial: {
        dialBackgroundColor: '#1B1B38',
    },
    clockFace: {
        clockFaceBackgroundColor: '#1B1B38',
        clockHandColor: '#AF87FF',
        clockFaceTimeInactiveColor: '#fff'
    }
};

  ngOnInit() {
  }

  async asistenciaHoy(boton: any) {

    boton.disabled=true;
    let listaAsistencias;
    let mesAgregado = false;

    const diaHoy = (formatDate(new Date(), 'yyyy/MM/dd', 'en').split('/')[2]);
    const mesHoy = (formatDate(new Date(), 'yyyy/MM/dd', 'en').split('/')[1]);
    // const diaHoy = '09';
    // const mesHoy = '03';


    //let idFechaHoy = await this.asisteciaService.GetIdFechaHoy('09','03') as any;
    for (let i = 0; i < this.asistencias.fechas.fechas.length; i++) {
      if (this.asistencias.fechas.fechas[i].dia == diaHoy && mesHoy === this.asistencias.fechas.fechas[i].mes) {
        mesAgregado = true;
        break;
      }
    }

    if (!mesAgregado) {

    let idFechaHoy = await this.asisteciaService.GetIdFechaHoy( diaHoy, mesHoy) as any;
    listaAsistencias = await this.asisteciaService.TomarAsistencia(this.curso.idMateria, parseInt(diaHoy), parseInt(mesHoy), this.curso.idCurso) as any;

    this.asistencias.fechas.fechas.push({
      idFecha: idFechaHoy.idFecha,
      dia: diaHoy.toString(),
      mes: mesHoy.toString(),
      estado: '2'
    });

      this.asistencias.fechas.fechas.sort(function(a, b) {return a.idFecha - b.idFecha; });
      this.displayedColumns = [];
      let mesEnLista;
      let duplicado;
      this.asistencias.fechas.fechas.forEach(fecha => {
        mesEnLista = parseInt(fecha.mes);
        for (let i = 0; i < this.displayedColumns.length; i++) {
          if (mesEnLista === parseInt(this.displayedColumns[i].nMes)) {
            duplicado = true;
            this.displayedColumns[i].nFechas++;
          }
        }
        if (!duplicado) {
            this.agregarMesLista(mesEnLista);
            duplicado = false;
        }
        duplicado = false;

      });
      this.displayedColumns.sort(function(a, b) {return a.nMes - b.nMes; });
      this.asistencias.estudiantes.forEach(estudiante => {
      for (let i = 0; i < listaAsistencias.length; i++) {
        if (listaAsistencias[i].idEstudiante == estudiante.idEstudiante) {
          estudiante.asistencias.asistencia.push({
            idAsistencia: listaAsistencias[i].idAsistencia,
            idFecha: idFechaHoy.idFecha,
            estado: '0'
          });
          break;
        }
      }
    });

    this.asistencias.estudiantes.forEach(estudiante => {
      estudiante.asistencias.asistencia.sort(function(a, b) {return a.idFecha - b.idFecha; });
    });

    }

    console.log(1);
    console.log(listaAsistencias);
    console.log(this.asistencias);

  }
  agregarMesLista(mes) {
    switch (mes) {
      case 1:
        this.displayedColumns.push({nMes: 1, mes: 'Enero', nFechas: 1});
        break;
      case 2:
          this.displayedColumns.push({nMes: 2, mes: 'Febrero', nFechas: 1});
          break;
      case 3:
          this.displayedColumns.push({nMes: 3, mes: 'Marzo', nFechas: 1});
          break;
      case 4:
          this.displayedColumns.push({nMes: 4, mes: 'Abril', nFechas: 1});
          break;
      case 5:
          this.displayedColumns.push({nMes: 5, mes: 'Mayo', nFechas: 1});
          break;
      case 6:
          this.displayedColumns.push({nMes: 6, mes: 'Junio', nFechas: 1});
          break;
      case 7:
          this.displayedColumns.push({nMes: 7, mes: 'Julio', nFechas: 1});
          break;
      case 8:
          this.displayedColumns.push({nMes: 8, mes: 'Agosto', nFechas: 1});
          break;
      case 9:
          this.displayedColumns.push({nMes: 9, mes: 'Septiembre', nFechas: 1});
          break;
      case 10:
          this.displayedColumns.push({nMes: 10, mes: 'Octubre', nFechas: 1});
          break;
      case 11:
          this.displayedColumns.push({nMes: 11, mes: 'Noviembre', nFechas: 1});
          break;
      case 12:
          this.displayedColumns.push({nMes: 12, mes: 'Diciembre', nFechas: 1});
          break;

      default:
        break;
    }
  }

  async asignar(idEstudiante: number, estado: string, input: any) {
    switch (estado) {
      case 'P':
        await this.asisteciaService.AgregarAsistencia(this.curso.idMateria,
                                                      this.diaHoy,
                                                      this.mesHoy,
                                                      idEstudiante, 1 );
        break;
      case 'F':
        await this.asisteciaService.AgregarAsistencia(this.curso.idMateria,
                                                      this.diaHoy,
                                                      this.mesHoy,
                                                      idEstudiante, 2 );
        break;
      case 'L':
        await this.asisteciaService.AgregarAsistencia(this.curso.idMateria,
                                                      this.diaHoy,
                                                      this.mesHoy,
                                                      idEstudiante, 3 );
        break;
      case 'A':
        await this.asisteciaService.AgregarAsistencia(this.curso.idMateria,
                                                      this.diaHoy,
                                                      this.mesHoy,
                                                      idEstudiante, 4 );
        break;
      default:
        estado = this.antiguo;
        break;
    }
    this.antiguo = estado;
    await this.asignarNuevo(input, estado);
  }

  async cambio(idAsistencia: number, estado: string, input: any) {
    let respuesta;
    switch (estado) {
      case 'P':
        respuesta = await this.asisteciaService.UpdateAsistencia(idAsistencia, 1) as object;
        break;
      case 'F':
        respuesta = await this.asisteciaService.UpdateAsistencia(idAsistencia, 2) as object;
        break;
      case 'L':
        respuesta = await this.asisteciaService.UpdateAsistencia(idAsistencia, 3) as object;
        break;
      case 'A':
        respuesta = await this.asisteciaService.UpdateAsistencia(idAsistencia, 4) as object;
        break;
      default:
        estado = this.antiguo;
        break;
    }

    if (respuesta.mensaje !== 1) {
      alert('Ha Ocurrido un error, contacte al administrador');
      estado = this.antiguo;
    }
    this.antiguo = estado;
    await this.reasignarAntiguo(input);
  }

  async inputAsistencia(input: any, antiguo: any) {
    input.style = 'border: 1px solid white;';
    this.antiguo = antiguo;
  }

  async reasignarAntiguo(input: any) {
    if (this.antiguo === undefined) {
      this.antiguo = '';
    }
    input.value = this.antiguo;
    switch (this.antiguo) {
      case '':
        input.style = 'border: 1px solid  #7956BF;';
        break;
      case 'P':
        input.style = 'border: 1px solid #00D68F;';
        break;
      case 'F':
        input.style = 'border: 1px solid #FF3D71;';
        break;
      case 'L':
        input.style = 'border: 1px solid #0095FF;';
        break;
      case 'A':
        input.style = 'border: 1px solid #FFAA00;';
        break;
      default:
        input.style = 'border: 1px solid white;';
        break;
    }
  }

  async asignarNuevo(input: any, estado: string) {
    input.value = estado;
    switch (estado) {
      case '':
        input.style = 'border: 1px solid  rgb(140, 0, 255);';
        break;
      case 'P':
        input.class = 'asistencia presente';
        break;
      case 'F':
        input.class = 'asistencia falta';
        break;
      case 'L':
        input.class = 'asistencia licencia';
        break;
      case 'A':
        input.class = 'asistencia atraso';
        break;
      default:
        input.style = 'border: 1px solid white;';
        break;
    }
  }

  async rangoFechas() {
    this.asistencias = await this.asisteciaService.GetAsistenciasEstudiantesRango(this.curso.idMateria,
      formatDate(this.rango.start, 'yyyy/MM/dd', 'en').split('/')[2],
      formatDate(this.rango.start, 'yyyy/MM/dd', 'en').split('/')[1],
      formatDate(this.rango.end, 'yyyy/MM/dd', 'en').split('/')[2],
      formatDate(this.rango.end, 'yyyy/MM/dd', 'en').split('/')[1]);
      console.log(this.asistencias);
  }



}
