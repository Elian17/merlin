import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CursoHorarioComponent } from './curso-horario.component';

describe('CursoHorarioComponent', () => {
  let component: CursoHorarioComponent;
  let fixture: ComponentFixture<CursoHorarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CursoHorarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CursoHorarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
