import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NuevoHorarioComponent } from './../../personal/nuevo-horario/nuevo-horario.component';
import { CursosService } from 'src/app/servicios/cursos.service';
import { HorarioService } from '../../servicios/horario.service';
import { PeriodosService } from '../../servicios/periodos.service';
import { MateriaService } from '../../servicios/materia.service';

import { Router } from '@angular/router';
import { Horario } from 'src/app/Clases/horario';


export interface DialogData {
 
  idPeriodo:number;
  idMateria:number;

  materias: any[];
  periodos: any[];
}

@Component({
  selector: 'app-curso-horario',
  templateUrl: './curso-horario.component.html',
  styleUrls: ['./curso-horario.component.scss']
})
export class CursoHorarioComponent implements OnInit {

  constructor(public dialog: MatDialog, private cursoService:CursosService, private horarioService:HorarioService, private periodoService:PeriodosService,
                private materiaSerivice: MateriaService) { }

  ngOnInit() {
    // this.listaHorarios();
    
    this.recargarCursos();
    // this.filtrarHorarios();
  }

  idColegio:any=null;
  idCurso:any=null;
  cursos:any= [];

  diaValue=1;

  horarios:any=[];

  idGest=1;

  idPeriodo:number;
  idMateria:number;
  

  selectHorarios=[
    {idHorario:1, idMateria:3, idCurso:9, idPeriodo:1, idGestion:1, dia:1},
    {idHorario:3, idMateria:1, idCurso:9, idPeriodo:1, idGestion:1, dia:2},
    {idHorario:4, idMateria:2, idCurso:9, idPeriodo:2, idGestion:1, dia:2},
    {idHorario:5, idMateria:4, idCurso:9, idPeriodo:1, idGestion:1, dia:3},
    {idHorario:12, idMateria:3, idCurso:9, idPeriodo:1, idGestion:1, dia:4},
    {idHorario:14, idMateria:1, idCurso:9, idPeriodo:3, idGestion:1, dia:4},
    {idHorario:10, idMateria:2, idCurso:9, idPeriodo:1, idGestion:1, dia:5},
  ];
  periodos:any=[];
  // periodos=[
  //   {idPeriodo:1,horaInicio:'06:30:00',horaFin:'07:30:00'},
  //   {idPeriodo:2,horaInicio:'07:31:00',horaFin:'08:35:00'},
  //   {idPeriodo:3,horaInicio:'09:05:00',horaFin:'10:00:00'},
  //   {idPeriodo:4,horaInicio:'10:05:00',horaFin:'11:45:00'},

  // ];

  materias:any=[];
  // materias=[
  //   {idMateria:1,nombre:'Ciencias Naturales'},
  //   {idMateria:2,nombre:'Matemáticas'},
  //   {idMateria:3,nombre:'Literatura'},
  //   {idMateria:4,nombre:'Física'},
  // ];

  colores =[
    {id:0, color:'#00D68F'},
    {id:1, color:'#A16EFF'},
    {id:2, color:'#ceb7ed'},
    {id:3, color:'#0095FF'},
    {id:4, color:'#FFAA00'},
    {id:5, color:'#FF3D71'},
    {id:6, color:'#ffa3c8'},
    {id:7, color:'#ffcc5a'},
    {id:8, color:'#bee27a'},
    {id:9, color:'#dfe0df'},
    {id:10, color:'#62baad'},
    
  ];

  lista:any=[];
  listaH:any=[];

  volverCursos() {
    this.idCurso = null;
  }

  seleccionCurso(curso: any) {
    this.idCurso = curso.idCurso;
    this.getPeriodos();
    this.getMaterias();
    
    this.getHorarios();
    
  }


  async getHorarios() {
    console.log(sessionStorage.getItem('colegio'));
    console.log(this.idCurso);

    this.listaH=await this.horarioService.GetHorariosCurso(this.idCurso,parseInt(sessionStorage.getItem('colegio')));
    console.log('Horarios: ');
    console.log(this.listaH);
    
  //   this.asistencias = await this.asistenciaService.GetAsistenciasEstudiantesRango(this.curso.idMateria, "0","0","0","0");
  //   let duplicado = false;
  //   let mesEnLista;
  //   this.asistencias.fechas.fechas.forEach(fecha => {
  //       mesEnLista = parseInt(fecha.mes);
  //       for (let i = 0; i < this.displayedColumns.length; i++) {
  //         if (mesEnLista == parseInt(this.displayedColumns[i].nMes)) {
  //           duplicado = true;
  //           this.displayedColumns[i].nFechas++;
  //         }
  //       }
  //       if (!duplicado) {
  //           this.agregarMesLista(mesEnLista);
  //           duplicado = false;
  //       }
  //       duplicado = false;

  //     });
  //   this.displayedColumns.sort(function(a,b){return a.nMes - b.nMes;});
  // this.asistencias.fechas.fechas.sort(function(a,b){return a.idFecha - b.idFecha;});
  //this.asistencias.fechas.fechas.sort(function(a,b){return a.mes - b.mes;});

  }

  async getPeriodos(){
    console.log(sessionStorage.getItem('colegio'));
    console.log(this.idCurso);
    this.periodos= await this.periodoService.GetPeriodosColegio(parseInt(sessionStorage.getItem('colegio')));
    // prePeriodos.forEach(periodo => {
    //   const per = new Periodo(periodo.idPeriodo,periodo.horaInicio.substring(0,5),periodo.horaFin.substring(0,5),parseInt(sessionStorage.getItem('colegio')),this.colores[this.periodos.length%4].color);
    //   this.periodos
    // });
    console.log('Periodos: ');
    console.log(this.periodos);
  }
  async getMaterias(){
    console.log(sessionStorage.getItem('colegio'));
    console.log(this.idCurso);
    this.materias= await this.materiaSerivice.GetMateriasCursoColegio(parseInt(sessionStorage.getItem('colegio')),this.idCurso);
    // prePeriodos.forEach(periodo => {
    //   const per = new Periodo(periodo.idPeriodo,periodo.horaInicio.substring(0,5),periodo.horaFin.substring(0,5),parseInt(sessionStorage.getItem('colegio')),this.colores[this.periodos.length%4].color);
    //   this.periodos
    // });
    console.log('Materias: ');
    console.log(this.materias);
  }


  async recargarCursos(){
    this.cursos =await this.cursoService.GetCursosColegio(parseInt(sessionStorage.getItem('colegio')));
    console.log(this.cursos);
    console.log('Colegio ID'+sessionStorage.getItem('colegio'));
    //this.dataSource = new MatTableDataSource(this.Cursos);
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(NuevoHorarioComponent, {
      width: '500px',
      data: { idPeriodo: this.idPeriodo, idMateria: this.idMateria, materias: this.materias,
              periodos: this.periodos}
    });

    dialogRef.afterClosed().subscribe(async result => {
      
      await this.horarioService.AgregarHorarioCurso(
        new Horario(1,parseInt(result.idMateria),this.idCurso,parseInt(result.idPeriodo),parseInt(sessionStorage.getItem('colegio')),this.diaValue)
      );
      //agregar Horario
      console.log(result);
      this.getHorarios();
    });
  }

  async removerMateria(idHorario:number){
    await this.horarioService.EliminarHorario(idHorario);
    console.log(idHorario);
  }

  // listaHorarios(){
  //   let preLista = this.selectHorarios.map(val => {
  //     return Object.assign({}, val, this.periodos.filter(p => p.idPeriodo === val.idPeriodo)[0]);
  //   });
  //   let listaFinal = preLista.map(val => {
  //     return Object.assign({}, val, this.materias.filter(m => m.idMateria === val.idMateria)[0]);
  //   });
   
    
  //   this.listaH=listaFinal;
  //   console.log(this.diaValue);
  //   console.log(listaFinal);

  //   // this.colores.forEach(element => {
  //   //   console.log(element.id%11);
  //   // });
    
  // }

  filtrarHorarios(){
    if (this.listaH.length>0 && this.diaValue!=null) {
      this.horarios=this.listaH.filter(h => h.dia==this.diaValue);

      console.log(this.horarios);
      this.cargarVista();
    }
    
  }
  
  cargarVista(){
    this.lista=[];
    //Ordenar Periodos por horas ASCENDENTE
    this.periodos.sort((val1, val2):any=> {
      return  new Date('2000-01-01T'+val1.horaInicio).getTime() - 
              new Date('2000-01-01T'+val2.horaInicio).getTime();
    });
    for (let i = 0; i < this.periodos.length; i++) {
      //Filtrar lista por periodo 
      const preL = this.listaH.filter(h => h.idPeriodo==this.periodos[i].idPeriodo);
      
      //Ordenar lista filtrada por DÍAS
      preL.sort(function(a, b) {
        return a.dia - b.dia;
      });
      console.log('Lista '+this.periodos[i].idPeriodo);
      console.log(preL);
      //Crear objeto para VISTA de Horarios
      const objLista = {  idPeriodo:this.periodos[i].idPeriodo ,
                        horaInicio:this.periodos[i].horaInicio, 
                        horaFin:this.periodos[i].horaFin,
                        lunes:'#323259',
                        martes:'#323259',
                        miercoles:'#323259',
                        jueves:'#323259',
                        viernes:'#323259',
                        sabado:'#323259',
                        mLunes:'',
                        mMartes:'',
                        mMiercoles:'',
                        mJueves:'',
                        mViernes:'',
                        mSabado:'',
                      };
      preL.forEach(element => {
          switch (element.dia) {
            case "1":
              console.log(element.idMateria+' '+element.nombre+' '+element.dia+ ' '+this.materias.length);
              objLista.lunes = this.colores[(element.idMateria%this.materias.length)].color;
              objLista.mLunes=element.nombre;
              break;
            case "2":
              objLista.martes = this.colores[(element.idMateria%this.materias.length)].color;
              objLista.mMartes=element.nombre;
              break;
            case "3":
              objLista.miercoles = this.colores[(element.idMateria%this.materias.length)].color;
              objLista.mMiercoles=element.nombre;
              break;
            case "4":
              objLista.jueves = this.colores[(element.idMateria%this.materias.length)].color;
              objLista.mJueves=element.nombre;
              break;
            case "5":
              objLista.viernes = this.colores[(element.idMateria%this.materias.length)].color;
              objLista.mViernes=element.nombre;
              break;
            case "6":
              objLista.sabado = this.colores[(element.idMateria%this.materias.length)].color;
              objLista.mSabado=element.nombre;
              break;

            default:
              break;
          }
          
      });
      // var eLista = {idPeriodo:this.periodos[i].idPeriodo ,horaInicio:this.periodos[i].horaInicio, horaFin:this.periodos[i].horaFin, materias:preL};
      //Agregar objeto a Lista para VISTA
      this.lista.push(objLista); 
    }
    console.log('Vista');
    console.log(this.lista);  
  }

}
