import { DialogTarea } from './../vista-notas/vista-notas.component';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogData } from 'src/app/usuarios/admin-inicio/admin-inicio.component';

@Component({
  selector: 'app-nueva-nota',
  templateUrl: './nueva-nota.component.html',
  styleUrls: ['./nueva-nota.component.scss']
})
export class NuevaNotaComponent implements OnInit {

  bimestre: number;
  descripcion: string;
  tipoNota: number;

  fechaNota: Date;


  constructor(public dialogRef: MatDialogRef<NuevaNotaComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogTarea) { }

  ngOnInit() {
  }
  onNoClick() {
    this.dialogRef.close();
  }

}
