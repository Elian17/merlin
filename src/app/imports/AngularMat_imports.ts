import { MatFormFieldModule } from '@angular/material/form-field';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material';
import { MatGridListModule, MatMenuModule } from '@angular/material';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTableModule, MatPaginatorModule, MatSortModule, MatDialogModule } from '@angular/material';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatTreeModule } from '@angular/material/tree';
import {MatListModule} from '@angular/material/list'; 

@NgModule({
    imports: [BrowserAnimationsModule, MatFormFieldModule,
        MatToolbarModule, MatIconModule, MatTableModule, MatTreeModule,
        MatButtonModule, MatCardModule, MatInputModule, MatSelectModule,
        MatGridListModule, MatMenuModule, MatSidenavModule, MatTabsModule,
        MatPaginatorModule, MatSortModule, MatTooltipModule, MatDialogModule, MatExpansionModule,
        MatListModule,
    ],
        
    exports: [BrowserAnimationsModule, MatFormFieldModule, MatExpansionModule,
        MatToolbarModule, MatIconModule, MatTableModule, MatTreeModule,
        MatButtonModule, MatCardModule, MatInputModule, MatSelectModule,
        MatGridListModule, MatMenuModule, MatSidenavModule, MatTabsModule,
        MatPaginatorModule, MatSortModule, MatTooltipModule, MatDialogModule],
})

export class Ang_Material_imports { }