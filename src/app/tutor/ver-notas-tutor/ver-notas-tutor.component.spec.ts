import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerNotasTutorComponent } from './ver-notas-tutor.component';

describe('VerNotasTutorComponent', () => {
  let component: VerNotasTutorComponent;
  let fixture: ComponentFixture<VerNotasTutorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerNotasTutorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerNotasTutorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
