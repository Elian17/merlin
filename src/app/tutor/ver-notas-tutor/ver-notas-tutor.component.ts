import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { SessionStorageService } from 'ngx-webstorage';
import { NotasService } from 'src/app/servicios/notas.service';

@Component({
  selector: 'app-ver-notas-tutor',
  templateUrl: './ver-notas-tutor.component.html',
  styleUrls: ['./ver-notas-tutor.component.scss']
})
export class VerNotasTutorComponent implements OnInit {
  Categoria:any;
  Materia:any;
  idCategoria:number;
  idMateria:number;
  Estudiante:any;
  idEstudiante:number
  Notas:any;
  displayedColumns: string[] = ['Materia','Descripcion','Nota', 'Tipo', 'Categoria','Fecha'];
  dataSource = this.Notas;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort,{static: false}) sort: MatSort;
  constructor(private notaService:NotasService,private sessionStorage:SessionStorageService) { }

  async ngOnInit() {
this.Estudiante = await this.notaService.GetEstudianteTutor(parseInt(sessionStorage.getItem('user')));

  }
  cargarInformacion(value:any):void {
    
    
    if(value !="undefined"){
     
      this.cargarInfoFiltro(value);
    }

  }

async cargarInfoFiltro(idEstudiante:number){
  this.Categoria=await this.notaService.GetCategoriasNotas();
    this.Materia = await this.notaService.GetMateriasEstudiante(idEstudiante);
}



  cargarNotas(value:any):void {
    console.log('Selected value is:sdsda ', value);
    
    if(value !="undefined"){
     
      this.cargarDatos(value);
    }

  }
  async cargarDatos(idMateria:number){
    this.Notas= await this.notaService.GetNotasIndividual(this.idEstudiante,idMateria,this.idCategoria);
     this.dataSource = new MatTableDataSource(this.Notas);
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
